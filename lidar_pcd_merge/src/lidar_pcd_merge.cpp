#include "lidar_pcd_merge/lidar_pcd_merge.h"

// Receives all PCDs, creates merged PCD and publishes it
void ROSLidarPCDMergeApp::PointsCallback(const sensor_msgs::PointCloud2::ConstPtr& parent_front_cloud_msg,
										const sensor_msgs::PointCloud2::ConstPtr& child_left_cloud_msg,
										const sensor_msgs::PointCloud2::ConstPtr& child_right_cloud_msg)
{
	pcl::PointCloud<PointT>::Ptr parent_front_cloud(new pcl::PointCloud<PointT>);
	pcl::PointCloud<PointT>::Ptr child_left_cloud(new pcl::PointCloud<PointT>);
	pcl::PointCloud<PointT>::Ptr child_right_cloud(new pcl::PointCloud<PointT>);
	pcl::PointCloud<PointT>::Ptr out_cloud(new pcl::PointCloud<PointT>);
  pcl::PointCloud<PointT>::Ptr localization_cloud(new pcl::PointCloud<PointT>);

	front_tf_ = FrameTransform("base_link",
																  parent_front_cloud_msg->header.frame_id);

	left_front_tf_ = FrameTransform("base_link",
																  child_left_cloud_msg->header.frame_id);
	right_front_tf_ = FrameTransform("base_link",
																 	 child_right_cloud_msg->header.frame_id);

	pcl::fromROSMsg(*parent_front_cloud_msg, *parent_front_cloud);
	pcl::fromROSMsg(*child_left_cloud_msg, *child_left_cloud);
	pcl::fromROSMsg(*child_right_cloud_msg, *child_right_cloud);

	std::vector<PointT> front_transformed_cloud(child_right_cloud->points.size());
	std::vector<PointT> left_transformed_cloud(child_left_cloud->points.size());
	std::vector<PointT> right_transformed_cloud(child_right_cloud->points.size());

	for (size_t i = 0; i < child_left_cloud->points.size(); i++)
	{
		pcl::PointXYZ transformed_point_left;
		left_transformed_cloud[i] = PointTransform(child_left_cloud->points[i], left_front_tf_);
		transformed_point_left.x = left_transformed_cloud[i].x;
		transformed_point_left.y = left_transformed_cloud[i].y;
		transformed_point_left.z = left_transformed_cloud[i].z;
		out_cloud->points.push_back(transformed_point_left);
    // double distance = sqrt(pow((0.0-transformed_point_left.x),2)+pow((0.0-transformed_point_left.y),2));
    // if (transformed_point_left.z <=1.65 && transformed_point_left.z >=ground_points_height_ && distance >=4)
    // {
    //   out_cloud->points.push_back(transformed_point_left);
    // }
		// if (transformed_point_left.z >=ground_points_height_ && distance >=4)
    //   localization_cloud->points.push_back(transformed_point_left);
	}

	for (size_t j = 0; j < child_right_cloud->points.size(); j++)
	{
		pcl::PointXYZ transformed_point_right;
		right_transformed_cloud[j] = PointTransform(child_right_cloud->points[j], right_front_tf_);
		transformed_point_right.x = right_transformed_cloud[j].x;
		transformed_point_right.y = right_transformed_cloud[j].y;
		transformed_point_right.z = right_transformed_cloud[j].z;
		out_cloud->points.push_back(transformed_point_right);
    // double distance = sqrt(pow((0.0-transformed_point_right.x),2)+pow((0.0-transformed_point_right.y),2));
    // if (transformed_point_right.z <=1.65 && transformed_point_right.z >=ground_points_height_ && distance >=4)
    // {
    //   out_cloud->points.push_back(transformed_point_right);
    // }
		// if (transformed_point_right.z >=ground_points_height_ && distance >=4)
    //   localization_cloud->points.push_back(transformed_point_right);
	}

	for (size_t k = 0; k < parent_front_cloud->points.size(); k++)
	{
		pcl::PointXYZ transformed_point_front;
		front_transformed_cloud[k] = PointTransform(parent_front_cloud->points[k], front_tf_);
		transformed_point_front.x = front_transformed_cloud[k].x;
		transformed_point_front.y = front_transformed_cloud[k].y;
		transformed_point_front.z = front_transformed_cloud[k].z;
		out_cloud->points.push_back(transformed_point_front);
    // double distance = sqrt(pow((0.0-transformed_point_front.x),2)+pow((0.0-transformed_point_front.y),2));
    // if (transformed_point_front.z <=1.65 && transformed_point_front.z >=ground_points_height_ && distance >=4)
    // {
    //   out_cloud->points.push_back(transformed_point_front);
    // }
		// if (transformed_point_front.z >=ground_points_height_ && distance >=4)
    //   localization_cloud->points.push_back(transformed_point_front);
	}

	//cropping roof points
	std::vector<int> indices;

	pcl::CropBox<PointT> roof(true);
	roof.setMin(Eigen::Vector4f (min_x_,min_y_, min_z_, 1));
	roof.setMax(Eigen::Vector4f (max_x_, max_y_, max_z_, 1));
	roof.setInputCloud(out_cloud);
	roof.filter(indices);

	pcl::PointIndices::Ptr roof_points(new pcl::PointIndices);
	for(int point : indices)
	 roof_points -> indices.push_back(point);

	pcl::ExtractIndices<PointT> extract;
	extract.setInputCloud (out_cloud);
	extract.setIndices (roof_points);
	extract.setNegative (true);
	extract.filter (*out_cloud);

	// OccupancyCropCloud(cropped_lidars_, out_cloud, parent_front_cloud_msg);

	if(ground_points_!=1)
	{
		pcl::PointCloud<PointT>::Ptr no_ground_points_cloud(new pcl::PointCloud<PointT>);
		pcl::SACSegmentation<PointT> seg;
    pcl::PointIndices::Ptr inliers {new pcl::PointIndices};
    pcl::ModelCoefficients::Ptr coefficients {new pcl::ModelCoefficients};

    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setMaxIterations(maxIterations_);
    seg.setDistanceThreshold(distanceThreshold_);

    seg.setInputCloud(out_cloud);
    seg.segment(*inliers, *coefficients);
    if(inliers->indices.size() == 0)
    {
      std::cout << "Could not estimate a planer model for the given dataset" <<std::endl;
    }
		else
		{
			pcl::ExtractIndices<PointT> extract;
	    extract.setInputCloud(out_cloud);
	    extract.setIndices(inliers);
	    extract.setNegative(true);
	    extract.filter(*no_ground_points_cloud);

			OccupancyCropCloud(cropped_lidars_, no_ground_points_cloud, parent_front_cloud_msg);

			// Publish merged PCD without ground points
			sensor_msgs::PointCloud2 merged_cloud_msg;
			pcl::toROSMsg(*no_ground_points_cloud, merged_cloud_msg);
			merged_cloud_msg.header = parent_front_cloud_msg->header;
			merged_cloud_msg.header.frame_id = "base_link";
			merged_cloud_msg.header.stamp = ros::Time::now();
			merged_lidars_.publish(merged_cloud_msg);
		}
	}
	else
	{
		OccupancyCropCloud(cropped_lidars_, out_cloud, parent_front_cloud_msg);

		// Publish merged PCD
		sensor_msgs::PointCloud2 merged_cloud_msg;
		pcl::toROSMsg(*out_cloud, merged_cloud_msg);
		merged_cloud_msg.header = parent_front_cloud_msg->header;
		merged_cloud_msg.header.frame_id = "base_link";
		merged_cloud_msg.header.stamp = ros::Time::now();
		merged_lidars_.publish(merged_cloud_msg);
	}
  localization_cloud->points.clear();
	out_cloud->points.clear();
}

//Cropping PCD for occupancy map
void ROSLidarPCDMergeApp::OccupancyCropCloud(const ros::Publisher& cropped_lidars, pcl::PointCloud<PointT>::ConstPtr new_out_cloud,
										const sensor_msgs::PointCloud2::ConstPtr& new_parent_front_cloud_msg)
{
	pcl::PointCloud<PointT>::Ptr cropped_cloud(new pcl::PointCloud<PointT>);

	pcl::PointCloud<PointT>::Ptr filtered_cloud(new pcl::PointCloud<PointT>);
	pcl::CropBox<PointT> occ_grid(true);
	occ_grid.setMin(Eigen::Vector4f (min_x_occ_,min_y_occ_, min_z_occ_, 1));
	occ_grid.setMax(Eigen::Vector4f (max_x_occ_, max_y_occ_, max_z_occ_, 1));
	occ_grid.setInputCloud(new_out_cloud);
	occ_grid.filter(*filtered_cloud);

	pcl::PointCloud<PointT>::Ptr low_cloud(new pcl::PointCloud<PointT>);
	pcl::CropBox<PointT> low_grid(true);
	low_grid.setMin(Eigen::Vector4f (-10.0,-10.0,0.0, 1));
	low_grid.setMax(Eigen::Vector4f (5.0,5.0,0.6, 1));
	low_grid.setInputCloud(new_out_cloud);
	low_grid.filter(*low_cloud);

	for (size_t m = 0; m < low_cloud->points.size(); m++)
	{
		pcl::PointXYZ transformed_point_high;
		// front_transformed_cloud[k] = PointTransform(parent_front_cloud->points[k], front_tf_);
		transformed_point_high.x = low_cloud->points[m].x;
		transformed_point_high.y = low_cloud->points[m].y;
		transformed_point_high.z = low_cloud->points[m].z;
		// out_cloud->points.push_back(transformed_point_front);
		cropped_cloud->points.push_back(transformed_point_high);
	}

	for (size_t n = 0; n < filtered_cloud->points.size(); n++)
	{
		pcl::PointXYZ transformed_point_low;
		// front_transformed_cloud[k] = PointTransform(parent_front_cloud->points[k], front_tf_);
		transformed_point_low.x = filtered_cloud->points[n].x;
		transformed_point_low.y = filtered_cloud->points[n].y;
		transformed_point_low.z = filtered_cloud->points[n].z;
		// out_cloud->points.push_back(transformed_point_front);
		cropped_cloud->points.push_back(transformed_point_low);
	}

	// Publish cropped merged PCD
	sensor_msgs::PointCloud2 cropped_cloud_msg;
	pcl::toROSMsg(*cropped_cloud, cropped_cloud_msg);
	cropped_cloud_msg.header = new_parent_front_cloud_msg->header;
	cropped_cloud_msg.header.frame_id = "base_link";
	cropped_cloud_msg.header.stamp = ros::Time::now();
	cropped_lidars.publish(cropped_cloud_msg);
  cropped_cloud->points.clear();

	//=============================================
	// fused_cropped lidar with no ground points
	pcl::PointCloud<PointT>::Ptr no_ground_cloud(new pcl::PointCloud<PointT>);

	pcl::PointCloud<PointT>::Ptr new_filtered_cloud(new pcl::PointCloud<PointT>);
	pcl::CropBox<PointT> non_grd(true);
	non_grd.setMin(Eigen::Vector4f (-50,-50, min_z_occ_, 1));
	non_grd.setMax(Eigen::Vector4f (50, 50, 15, 1));
	non_grd.setInputCloud(new_out_cloud);
	non_grd.filter(*new_filtered_cloud);


	// Publish cropped merged PCD
	sensor_msgs::PointCloud2 no_ground_cloud_msg;
	pcl::toROSMsg(*no_ground_cloud, no_ground_cloud_msg);
	no_ground_cloud_msg.header = new_parent_front_cloud_msg->header;
	no_ground_cloud_msg.header.frame_id = "base_link";
	no_ground_cloud_msg.header.stamp = ros::Time::now();
	merged_lidars_no_ground_.publish(no_ground_cloud_msg);
  no_ground_cloud->points.clear();
}

// Finds tranform between two frames (if it exists) based on the tfs being published
tf::StampedTransform
ROSLidarPCDMergeApp::FrameTransform(const std::string &in_target_frame, const std::string &in_source_frame)
{
	tf::StampedTransform lidar_transform;
	try
	{
		transform_listener_->lookupTransform(in_target_frame, in_source_frame, ros::Time(0), lidar_transform);
		return lidar_transform;
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("[%s] %s", __APP_NAME__, ex.what());
	}
	return lidar_transform;
}

// Receives a point from the different child lidar PCDs and tranforms them to parent frame
pcl::PointXYZ
ROSLidarPCDMergeApp::PointTransform(const PointT &in_point, const tf::StampedTransform &in_transform)
{
	tf::Vector3 tf_point(in_point.x, in_point.y, in_point.z);
	tf::Vector3 tf_point_t = in_transform * tf_point;
	return PointT(tf_point_t.x(), tf_point_t.y(), tf_point_t.z());
}

//Initializes subscribers (synchronizes them) and publishers.
void ROSLidarPCDMergeApp::InitializeROSIo(ros::NodeHandle &in_private_handle)
{
	//get params
	std::string left_lidar_str,right_lidar_str, front_lidar_str, fused_lidar_str = "/fused_lidar", cropped_lidar_str = "/fused_cropped_lidar";
	std::string name_space_str = ros::this_node::getNamespace();
	if (name_space_str != "/")
	{
		left_lidar_str = name_space_str + left_lidar_str;
		right_lidar_str = name_space_str + right_lidar_str;
		front_lidar_str = name_space_str + front_lidar_str;
		fused_lidar_str = name_space_str + fused_lidar_str;
	}

	in_private_handle.param<std::string>("left_lidar_points", left_lidar_str, "/lidar_left/velodyne_points");
	ROS_INFO("[%s] left_lidar_points: %s",__APP_NAME__, left_lidar_str.c_str());

	in_private_handle.param<std::string>("right_lidar_points", right_lidar_str, "/lidar_right/velodyne_points");
	ROS_INFO("[%s] right_lidar_points: %s",__APP_NAME__, right_lidar_str.c_str());

	in_private_handle.param<std::string>("front_lidar_points", front_lidar_str, "/lidar_front/velodyne_points");
	ROS_INFO("[%s] front_lidar_points: %s",__APP_NAME__, front_lidar_str.c_str());

	in_private_handle.param<bool>("is_cloud_colored", cloud_is_colored_, false);
	ROS_INFO("[%s] is_cloud_colored: %s",__APP_NAME__, cloud_is_colored_ ? "true" : "false");

	in_private_handle.param<bool>("ground_points", ground_points_, false);
	ROS_INFO("[%s] cloud has ground points: %s",__APP_NAME__, ground_points_ ? "true" : "false");

	in_private_handle.param<double>("ground_points_filter_height", ground_points_height_, 0.6);
	ROS_INFO("[%s] height threshold for ground filter %f",__APP_NAME__, ground_points_height_);

	in_private_handle.param<int>("max_iterations_ground_filter", maxIterations_, 10);
	ROS_INFO("[%s] max iterations for ground filter %d",__APP_NAME__, maxIterations_);

	in_private_handle.param<float>("distance_threshold_ground_filter", distanceThreshold_, 0.1);
	ROS_INFO("[%s] distance threshold for ground filter %f",__APP_NAME__, distanceThreshold_);

	in_private_handle.param<double>("roof_crop_box_min_x", min_x_, 3.5);
	ROS_INFO("[%s] roof crop box min x: %f",__APP_NAME__, min_x_);

	in_private_handle.param<double>("roof_crop_box_min_y", min_y_, 3.5);
	ROS_INFO("[%s] roof crop box min x: %f",__APP_NAME__, min_y_);

	in_private_handle.param<double>("roof_crop_box_min_z", min_z_, 3.5);
	ROS_INFO("[%s] roof crop box min x: %f",__APP_NAME__, min_z_);

	in_private_handle.param<double>("roof_crop_box_max_x", max_x_, 3.5);
	ROS_INFO("[%s] roof crop box min x: %f",__APP_NAME__, max_x_);

	in_private_handle.param<double>("roof_crop_box_max_y", max_y_, 3.5);
	ROS_INFO("[%s] roof crop box min x: %f",__APP_NAME__, max_y_);

	in_private_handle.param<double>("roof_crop_box_max_z", max_z_, 3.5);
	ROS_INFO("[%s] roof crop box min x: %f",__APP_NAME__, max_z_);

	in_private_handle.param<double>("occupancy_grid_crop_box_min_x", min_x_occ_, -30.0);
	ROS_INFO("[%s] occupancy grid crop box min x: %f",__APP_NAME__, min_x_occ_);

	in_private_handle.param<double>("occupancy_grid_crop_box_min_y", min_y_occ_, -15.0);
	ROS_INFO("[%s] occupancy grid crop box min y: %f",__APP_NAME__, min_y_occ_);

	in_private_handle.param<double>("occupancy_grid_crop_box_min_z", min_z_occ_, 0.0);
	ROS_INFO("[%s] occupancy grid crop box min z: %f",__APP_NAME__, min_z_occ_);

	in_private_handle.param<double>("occupancy_grid_crop_box_max_x", max_x_occ_, 30.0);
	ROS_INFO("[%s] occupancy grid crop box max x: %f",__APP_NAME__, max_x_occ_);

	in_private_handle.param<double>("occupancy_grid_crop_box_max_y", max_y_occ_, 15.0);
	ROS_INFO("[%s] occupancy grid crop box max y: %f",__APP_NAME__, max_y_occ_);

	in_private_handle.param<double>("occupancy_grid_crop_box_max_z", max_z_occ_, 5.0);
	ROS_INFO("[%s] occupancy grid crop box max z: %f",__APP_NAME__, max_z_occ_);

	//generate subscribers and sychronizers
	cloud_left_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
	                                                                                     left_lidar_str, 10);
	ROS_INFO("[%s] Subscribing to... %s",__APP_NAME__, left_lidar_str.c_str());

	cloud_right_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
	                                                                                     right_lidar_str, 10);
	ROS_INFO("[%s] Subscribing to... %s",__APP_NAME__, right_lidar_str.c_str());

	cloud_front_subscriber_ = new message_filters::Subscriber<sensor_msgs::PointCloud2>(node_handle_,
	                                                                                     front_lidar_str, 10);
	ROS_INFO("[%s] Subscribing to... %s",__APP_NAME__, front_lidar_str.c_str());

	merged_lidars_ = node_handle_.advertise<sensor_msgs::PointCloud2>(fused_lidar_str, 1);
	ROS_INFO("[%s] Publishing merged PointCloud to... %s",__APP_NAME__, fused_lidar_str.c_str());

	cropped_lidars_ = node_handle_.advertise<sensor_msgs::PointCloud2>(cropped_lidar_str, 1);
	ROS_INFO("[%s] Publishing cropped merged PointCloud to... %s",__APP_NAME__, cropped_lidar_str.c_str());

	merged_lidars_no_ground_ = node_handle_.advertise<sensor_msgs::PointCloud2>("/fused_lidar_no_ground", 1);
	ROS_INFO("[%s] Publishing cropped merged PointCloud witn no ground points to... %s",__APP_NAME__, "/fused_lidar_no_ground");

// Syncing all subscribers
	cloud_synchronizer_ =
			new message_filters::Synchronizer<SyncPolicyT>(SyncPolicyT(100),
																									 	 *cloud_front_subscriber_,
																										 *cloud_left_subscriber_,
																										 *cloud_right_subscriber_);
	cloud_synchronizer_->registerCallback(boost::bind(&ROSLidarPCDMergeApp::PointsCallback, this, _1, _2, _3));
}
void ROSLidarPCDMergeApp::Run()
{
	ros::NodeHandle private_node_handle("~");

	tf::TransformListener transform_listener;
	transform_listener_ = &transform_listener;

	ROS_INFO("[%s] Ready. Waiting for data...", __APP_NAME__);

	InitializeROSIo(private_node_handle);

	ros::spin();

	ROS_INFO("[%s] END", __APP_NAME__);
}
ROSLidarPCDMergeApp::ROSLidarPCDMergeApp()
{
	cloud_is_colored_ = false;
}
