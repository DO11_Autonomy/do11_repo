#include "mazda/mazda.h"

Mazda::Mazda(): nh_(""){
  mazda_pub_ =  nh_.advertise<sensor_msgs::Joy>("/joy_mazda", 1);

  joy_sub_ =  nh_.subscribe("joy_orig", 1000, &Mazda::joyCallback, this);
  timer1 = nh_.createTimer(ros::Duration(0.1), &Mazda::timerCallback, this);
};

Mazda::~Mazda(){};

void Mazda::joyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
  current_joy_ = std::make_shared<sensor_msgs::Joy>(*msg);
  steer_ = current_joy_->axes[3];
  velocity_ = current_joy_->axes[1];
}

void Mazda::timerCallback(const ros::TimerEvent&)
{
  sensor_msgs::Joy joy_msg{};
//  joy_msg.axes[3] = steer_;
//  joy_msg.axes[1] = velocity_;
  joy_msg.axes = {0,velocity_,0,steer_,0,0,0,0};
  mazda_pub_.publish(joy_msg);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "mazda_node");
  Mazda obj;
  ros::spin();

  return 0;
};



