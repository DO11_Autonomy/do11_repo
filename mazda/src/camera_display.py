#! /usr/bin/env python
import rospy
from sensor_msgs.msg import Image
import cv2
import matplotlib.pyplot as plt
from cv_bridge import CvBridge, CvBridgeError

class camera_display:
    def __init__(self):
        self.bridge = CvBridge()
        self.front_left_image = rospy.Subscriber("/camera_front_left/image_rectified", Image, self.front_left_callback)
        self.front_right_image = rospy.Subscriber("/camera_front_right/image_rectified", Image, self.front_right_callback)
        self.right_image = rospy.Subscriber("/camera_right/image_rectified", Image, self.right_callback)
        self.left_image = rospy.Subscriber("/camera_left/image_rectified", Image, self.left_callback)
        self.rear_image = rospy.Subscriber("/camera_rear/image_rectified", Image, self.rear_callback)

    def front_left_callback(self, img):
        #self.lock.acquire()
        global img1, scale_percent, width, height, dim, scale_percent
        cv_image = self.bridge.imgmsg_to_cv2(img, desired_encoding='bgr8')
        #img1 = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        img1 = cv_image
        scale_percent = 70 # percent of original size
        width = int(img1.shape[1] * scale_percent / 100)
        height = int(img1.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        img1 = cv2.resize(img1, dim, interpolation = cv2.INTER_AREA)
        font = cv2.FONT_HERSHEY_SIMPLEX
        # Create a black image
        cv2.putText(img1,'front_left',(0,50), font, 1,(255,255,255),2)

    def front_right_callback(self, img):
        #self.lock.acquire()
        global img2
        cv_image = self.bridge.imgmsg_to_cv2(img, desired_encoding='bgr8')
        #img2 = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        img2 = cv_image
        # scale_percent = 70 # percent of original size
        # width = int(img2.shape[1] * scale_percent / 100)
        # height = int(img2.shape[0] * scale_percent / 100)
        # dim = (width, height)
        # resize image
        img2 = cv2.resize(img2, dim, interpolation = cv2.INTER_AREA)
        font = cv2.FONT_HERSHEY_SIMPLEX
        # Create a black image
        cv2.putText(img2,'front_right',(0,50), font, 1,(255,255,255),2)

    def left_callback(self, img):
        #self.lock.acquire()
        global img4
        cv_image = self.bridge.imgmsg_to_cv2(img, desired_encoding='bgr8')
        #img4 = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        img4 = cv_image
        # scale_percent = 70 # percent of original size
        # width = int(img4.shape[1] * scale_percent / 100)
        # height = int(img4.shape[0] * scale_percent / 100)
        # dim = (width, height)
        # resize image
        img4 = cv2.resize(img4, dim, interpolation = cv2.INTER_AREA)
        font = cv2.FONT_HERSHEY_SIMPLEX
        # Create a black image
        cv2.putText(img4,'left',(0,50), font, 1,(255,255,255),2)

    def right_callback(self, img):
        #self.lock.acquire()
        global img3
        cv_image = self.bridge.imgmsg_to_cv2(img, desired_encoding='bgr8')
        #img3 = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        img3 = cv_image
        # scale_percent = 70 # percent of original size
        # width = int(img3.shape[1] * scale_percent / 100)
        # height = int(img3.shape[0] * scale_percent / 100)
        # dim = (width, height)
        # resize image
        img3 = cv2.resize(img3, dim, interpolation = cv2.INTER_AREA)
        font = cv2.FONT_HERSHEY_SIMPLEX
        # Create a black image
        cv2.putText(img3,'right',(0,50), font, 1,(255,255,255),2)

    def rear_callback(self, img):
        #self.lock.acquire()
        global img5
        cv_image = self.bridge.imgmsg_to_cv2(img, desired_encoding='bgr8')
        #img5 = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        img5 = cv_image
        # scale_percent = 70 # percent of original size
        # width = int(img3.shape[1])
        # height = int(img3.shape[0])
        # dim = (width, height)
        # resize image
        img5 = cv2.resize(img5, dim, interpolation = cv2.INTER_AREA)
        font = cv2.FONT_HERSHEY_SIMPLEX
        # Create a black image
        cv2.putText(img5,'rear',(0,50), font, 1,(255,255,255),2)
        ###########################shift this part to the last exected callback
        imh1 = cv2.hconcat([img4,img1,img2,img3]) #1st row of display
        imh2 = cv2.hconcat([img2,img3,img5,img4])
        #imh3 = cv2.hconcat([img1,img3])
        imv = cv2.vconcat([imh1,imh2])
        #imh2 = cv2.hconcat([img4,img5,img3]) #2 row of display
        #imv = cv2.vconcat([imh,imh2]) #combining both rows of display
        cv2.imshow("camera_display",imv)
        cv2.waitKey(1)
        ##############################

    # def display():
    #     imh = cv2.hconcat([img1,img2])
    #     # imh2 = cv2.hconcat([img4,img5,img3])
    #     # imv = cv2.vconcat([imh,imh2])
    #     cv2.imshow("camera_display",imh)
    #     cv2.waitKey(1)


def main():
    ic = camera_display()
    rospy.init_node('camera_display', anonymous=True)
    rospy.spin()

    #prev_objects = []

if __name__ == '__main__':
    main()
