
clear; clc; close all

%% initialize rosnode
rosshutdown
instrreset
rosinit


%% subscribe to high level controller
global joytopic
joytopic = rossubscriber('/joy',@joycallback);


%% Connect to motor driver
global roboteq
try
    roboteq = serial('/dev/ttyACM2', 'BaudRate', 9600);
    fopen(roboteq);
catch
    roboteq = serial('/dev/ttyACM1', 'BaudRate', 9600);
    fopen(roboteq);

end     

%% ROS callback function
function joycallback(~,message)
    global joytopic
    global roboteq
    global spd_tgt_topic
    global steer_tgt_topic
    global a_tgt
    
    steer_tgt_topic = int16(joytopic.LatestMessage.Axes(4)*930); % Put your own steering conronl here.
    steering = max(min(steer_tgt_topic,930),-930); %% This line needs to be kept to avoid damaging the system for steering.
    fwrite(roboteq,strcat(['!g 1 ',num2str(steering)],hex2dec('0d'))); % Write to motor driver
    
%     Initial position for throttle motor is with the pole barely touching
%     the accelerator pedal
    spd_tgt_topic = double(joytopic.LatestMessage.Axes(2)*1000);
    throttle = max(min(spd_tgt_topic,100),-450); %% This line needs to be kept to avoid damaging the system.
    fwrite(roboteq,strcat(['!g 2 ',num2str(throttle)],hex2dec('0d'))); % Write to motor driver
    
    fprintf('steer_topic = %d   steering = %d   speed_topic = %d   throttle = %d \n',steer_tgt_topic, steering, spd_tgt_topic, throttle)
    
end
