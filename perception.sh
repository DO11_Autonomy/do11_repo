#!/bin/bash

#this shell file launches #all lidars #all cameras with rectification

#cmd1="sudo modprobe usbcore usbfs_memory_mb=1500"
cmd1="roslaunch mazda mazda_all_lidars.launch"
cmd2="roslaunch mazda mazda_all_cameras.launch"
cmd3="sleep 3"
cdm4="roslaunch lidar_pcd_merge lidar_pcd_merge.launch"
cmd5="cd ~/do11_workspace/src/do11_repo/mazda/src"
cmd6="python camera_display.py"
cmd7="roslaunch lidar_pcd_merge lidar_pcd_merge.launch"
cmd8="roslaunch cam_lidar_fusion cam_lidar_fusion.launch"
cm9="sleep 6"
cm10="roslaunch mazda camera_front_right.launch"
cm11="roslaunch mazda camera_front_left.launch"
cm12="roslaunch mazda camera_right.launch"
cm13="roslaunch mazda camera_left.launch"
cm14="roslaunch mazda camera_rear.launch"
gnome-terminal --tab --command="bash -c '$cmd1; $SHELL '"\
	       --tab --command="bash -c '$cmd3; $cmd10; $cmd9; $cmd11; $cmd9; $cmd12; $cmd9; $cmd13; $cmd9; $cmd14; $SHELL '"\
	       --tab --command="bash -c '$cmd3; $cmd7; $SHELL '"\
	       --tab --command="bash -c '$cmd5; $cmd6; $SHELL '"\
	       --tab --command="bash -c '$cmd3; $cmd8; $SHELL '"\


