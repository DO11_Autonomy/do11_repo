#include <Wire.h>
#include "Pozyx.h"
#include "Pozyx_definitions.h"


////////////////////////////////////////////////
////////// APPLICATION VARIABLES ///////////////
////////////////////////////////////////////////
uint8_t  calibration_status = 0;
uint8_t  calib_status;
uint16_t network_id = 0;

uint8_t  num_anchors = 4; // the number of anchors to use for the constellation

// urban systems outdoors - dec 26, 2018
uint16_t   anchors[4] = {0x6e19, 0x6e5c, 0x6e39, 0x6e0d}; // the network id of the anchors: change these to the network ids of your anchors.
int32_t  anchors_x[4] = { 0, 16000,  16850,  -8680}; // anchor x-coordinates in mm
int32_t  anchors_y[4] = {0, 0, -19500, -26480}; // anchor y-coordinates in mm
int32_t  anchors_z[4] = {   3060,  3070,   3100,   1800}; // anchor z-coordinates in mm (not critical for ground vehicles)
//int32_t  anchors_z[4] = {   4280,  4280,   4280,   4280}; // anchor z-coordinates in mm (not critical for ground vehicles)
//uint8_t  algorithm = POZYX_POS_ALG_UWB_ONLY;   // positioning algorithm. Try POZYX_POS_ALG_TRACKING for fast moving objects vs POZYX_POS_ALG_UWB_ONLY
uint8_t  algorithm = POZYX_POS_ALG_TRACKING;   // positioning algorithm. Try POZYX_POS_ALG_TRACKING for fast moving objects vs POZYX_POS_ALG_UWB_ONLY
uint8_t  dimension = POZYX_2_5D;                 // positioning dimension (2D is more accurate for ground vehicles)
int32_t  height = 400;                         // height of the mobile device, only required in 2.5D or 3D positioning
uint16_t remote_id = NULL;                     // no remote devices
/////////////////////////////////////////////////

void setup(){
  Serial.begin(115200);
  delay(1000);
  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println(F("ERROR: Unable to connect to UWB shield"));
    Serial.println(F("Reset required"));
    delay(100);
    abort();
  }
  Pozyx.clearDevices(NULL); // clear previous devices in device list
  setAnchors();
  Pozyx.setPositionAlgorithm(algorithm, dimension, remote_id);
  // jd - filter strength is the second parameter (values=0..15) "FILTER_TYPE_MOVINGAVERAGE, 15" was used for the video shoot
  Pozyx.setPositionFilter(FILTER_TYPE_MOVINGAVERAGE, 15);  
  //Pozyx.setPositionFilter(FILTER_TYPE_MOVINGAVERAGE, 15); 
  //Pozyx.setPositionFilter(FILTER_TYPE_MOVINGMEDIAN, 15); 
  //Pozyx.setPositionFilter(FILTER_TYPE_FIR, 15);  
  //Pozyx.setPositionFilter(FILTER_TYPE_NONE, 0);  
  delay(2000);
}

void loop(){
  coordinates_t position;
  euler_angles_t hdg;
  int status = Pozyx.doPositioning(&position, dimension, height);
  Pozyx.getEulerAngles_deg(&hdg,NULL);
  if (status == POZYX_SUCCESS){
    printCoords_Hdg(position, hdg);
  }
}

// print coordinates and heading (direction the beacon is pointing relative to anchor constellation)
void printCoords_Hdg(coordinates_t coor, euler_angles_t heading){
  char temp_string[12];
  euler_angles_t temp_angle = heading;
  sprintf(temp_string, "%9d", coor.x);
  Serial.print(temp_string); 
  Serial.print(",");
  sprintf(temp_string, "%9d", coor.y);
  Serial.print(temp_string);  
  Serial.print(",");
   sprintf(temp_string, "%9d", coor.z);  
  Serial.print(temp_string); 
  Serial.print(",");
  sprintf(temp_string, "%9d", (int) (heading.heading));
  Serial.println(temp_string);
  //Serial.print("LED indicated error: ");
  //Serial.println(POZYX_ERRORCODE); 
}

// function to programmatically set anchor coordinates
void setAnchors(){
  for(int i = 0; i < num_anchors; i++){
    device_coordinates_t anchor;
    anchor.network_id = anchors[i];
    anchor.flag = 0x1;
    anchor.pos.x = anchors_x[i];
    anchor.pos.y = anchors_y[i];
    anchor.pos.z = anchors_z[i];
    Pozyx.addDevice(anchor, NULL);
  }
  if (num_anchors > 4){
    Pozyx.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, num_anchors, NULL);
  }
}
