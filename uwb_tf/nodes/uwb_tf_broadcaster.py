#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import rospy
import tf
from sensor_msgs.msg import Image,LaserScan,Joy
from geometry_msgs.msg import Pose,Twist,PoseStamped
import sys


class tf_transfer:
    def __init__(self):
        self.msg = PoseStamped()
        self.uwb_sub=rospy.Subscriber('uwb/pose',PoseStamped,self.uwb_callback)
        self.location=PoseStamped()

    def uwb_callback(self,uwb_data):
        rospy.loginfo("Receiving Information")# Test the info inflow
        self.msg=uwb_data
        self.loc_x=self.msg.pose.position.x
	    self.loc_y=-self.msg.pose.position.y
    	self.loc_z=self.msg.pose.position.z

    	self.angle_x=self.msg.pose.orientation.x
    	self.angle_y=self.msg.pose.orientation.y
    	self.angle_z=self.msg.pose.orientation.z
    	self.angle_w=self.msg.pose.orientation.w
	#handle_uwb_tf(self.loc_x, self.loc_y, self.loc_z, self.angle_x, self.angle_y, self.angle_z, self.angle_w)
	#handle_uwb_tf(self)
	br = tf.TransformBroadcaster()
        br.sendTransform((self.loc_x, self.loc_y, self.loc_z),
                     (self.angle_x, self.angle_y, self.angle_z, self.angle_w),
                     rospy.Time.now(),
                     "uwb_frame",
                     "world")
        rospy.loginfo("Publishing Information")# Test the info inflow

    def handle_uwb_tf(self):
        br = tf.TransformBroadcaster()
        br.sendTransform((self.loc_x, self.loc_y, self.loc_z),
                     (self.angle_x, self.angle_y, self.angle_z, self.angle_w),
                     rospy.Time.now(),
                     "uwb_frame",
                     "world")



if __name__ == "__main__":
    rospy.init_node('tf_broadcaster')
    C = tf_transfer()
    r = rospy.Rate(40)
    while not rospy.is_shutdown():
        # r.sleep()
        rospy.spin()
