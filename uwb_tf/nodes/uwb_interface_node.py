#!/usr/bin/env python
import rospy
import serial
import numpy as np #Used for converting euler angles to quaternions
import math #For atan2
from geometry_msgs.msg import PoseStamped, Twist
from std_msgs.msg import Bool, UInt64
from nav_msgs.msg import Odometry
#Converts roll pitch and yaw to quaternions
def euler_to_quaternion(yaw, pitch, roll):
        qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
        qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
        qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
	return (qx, qy, qz, qw)

#Extracts the position values from the serial packet string. 
def extract_serial_data(raw):	
	raw.replace("\r", "")#Remove /r and /n from the input.
	raw.replace("\n", "")
	values = raw.split(",")#Split the values into their separate parts
	try: #Check for errors stemming from a malformed packet and handle them	
		values = [float(i) for i in values] #Convert string values to float.
	except ValueError:
		rospy.logerr("Unkown error parsing packet.")
		return False	
	if len(values) != 4:
		rospy.logerr("Did not parse four serial values.")	
		return False
	else:
		return values

def normalize_angle(a):
	if a <= math.pi and a > -math.pi:
		return a
	elif a <= -math.pi:
		return a + (2*math.pi)
	else:
		return a - (2*math.pi)

#Computes the distance between two sets of coordinates
def dist(xa, ya, xb, yb):
	return np.sqrt( ((xa - xb) * (xa - xb)) + ((ya - yb) * (ya - yb)) )

#Previous calibration point
previous_calibration_pose = (0, 0, 0)
#Angle offset
calibrated_offset = 0
calibrate_first_run = True
last_calibration = 0  
#Calibrates heading based on movement angle, does not work when traveling backwards
def calibrate_data(data):
	global previous_calibration_pose
	global calibrated_offset
	global calibrate_first_run
	global last_calibration
	global calibrated_pub
	global angular_velocity
	x = data[0]
	y = data[1]
	theta = data[3]	
	if calibrate_first_run:
		previous_calibration_pose = (x, y, theta)
		calibrate_first_run = False
	if data == False:
		return
	rospy.logdebug("Distance from calibration: %f", dist(x, y, previous_calibration_pose[0], previous_calibration_pose[1]))
	if dist(x, y, previous_calibration_pose[0], previous_calibration_pose[1]) > rospy.get_param('calibration_distance', 3.00):
		tmp_x = x - previous_calibration_pose[0]
		tmp_y = y - previous_calibration_pose[1]			
		real_heading = math.atan2(tmp_y, tmp_x)
		calibrated_offset = normalize_angle(real_heading - theta)
		previous_calibration_pose = (x, y, data[2])
		rospy.loginfo("Updated calibrated heading offset: %f", calibrated_offset)
		last_calibration = rospy.get_time()
	if angular_velocity > 0.15 or angular_velocity < -0.15:
		previous_calibration_pose = (x, y, theta)
	calibrated_msg = UInt64()
	calibrated_msg.data = abs(rospy.get_time() - last_calibration)
	calibrated_pub.publish(calibrated_msg)

X = 0
Y = 1
Z = 2
T = 3
fix_previous_data = [0, 0, 0, 0]
fix_previous_time = 0
fix_max_delta = [0.1, 0.1, 0.1, 0.1]
fix_first_run = True
fix_previous_state = True

def diff_radians(a, b):
	diff = abs(a - b)
	if diff > 6.2:
		rospy.logwarn("Close to 2pi")
		diff = 0
	return diff
def has_fix(data):
	global fix_first_run
	global fix_previous_data
	global fix_previous_time
	global fix_previous_state
	global odom_velocities, odom_velocities_set
	global X, Y, Z, T
	fix_output = True
	current_time = rospy.get_time()
	if fix_first_run:
		fix_previous_data = data
		fix_first_run = False
		if not odom_velocities_set:
			fix_max_delta[X] = rospy.get_param('max_delta_x', 6) #Max speed threshold in meters/second
			fix_max_delta[Y] = rospy.get_param('max_delta_y', 6)
			fix_max_delta[Z] = rospy.get_param('max_delta_z', 6)
		else:
			fix_max_delta[X] = odom_velocities[X] * rospy.get_param('odom_delta_multiplier_x', 1.3)
			fix_max_delta[Y] = odom_velocities[Y] * rospy.get_param('odom_delta_multiplier_y', 1.3)
			fix_max_delta[Z] = odom_velocities[Z] * rospy.get_param('odom_delta_multiplier_z', 1.3)			
		fix_max_delta[T] = rospy.get_param('max_delta_theta', 4 * math.pi) #Max rotation threshold in radians/second
		fix_previous_time = current_time
		fix_previous_state = True
		return True #Assume the first position received is good, subsequent ones will be checked.
	time_delta = abs(current_time - fix_previous_time) #Time since previous packet received
	if time_delta > rospy.get_param('fix_timeout', 0.5):
		rospy.logwarn("Fix lost: Time since last fix is more than allowed: %f, max: %f", time_delta, rospy.get_param('fix_timeout', 0.5))	
		fix_output = False
	position_deltas = [0, 0, 0, 0]
	position_deltas[X] = abs(data[X] - fix_previous_data[X]) / time_delta
	position_deltas[Y] = abs(data[Y] - fix_previous_data[Y]) / time_delta
	position_deltas[Z] = abs(data[Z] - fix_previous_data[Z]) / time_delta
	position_deltas[T] = abs(diff_radians(data[T], fix_previous_data[T])) / time_delta
	over_max = False
	over_max = over_max or (position_deltas[X] > fix_max_delta[X])
	over_max = over_max or (position_deltas[Y] > fix_max_delta[Y])
	over_max = over_max or (position_deltas[Z] > fix_max_delta[Z])
	#over_max = over_max or (position_deltas[T] > fix_max_delta[T])
	if fix_output and over_max == True:
		warnings = [position_deltas[X] > fix_max_delta[X], position_deltas[Y] > fix_max_delta[Y], position_deltas[Z] > fix_max_delta[Z], position_deltas[T] > fix_max_delta[T]]
		rospy.logwarn("Fix lost: position delta(s) exceeded.")
		#rospy.logwarn("Position Delta: %f" % position_deltas[T])
		#rospy.logwarn("Time delta: %f" % time_delta)
		#rospy.logwarn("Position delta without time: %f, %f" % (data[T], fix_previous_data[T]))
		fix_output = False
	fix_previous_data = data
	fix_previous_time = current_time
	if not fix_previous_state and fix_output:
		rospy.logwarn("Fix regained.")
	return fix_output		
	

#Publishes the fix state as provided and sleeps to maintain the rate.
def do_loop(fix_state):
	global rate
	
	publish_fix(fix_state)
	rate.sleep()

def publish_fix(state):
	global fix_pub
	fix_msg = Bool()
	fix_msg.data = state
	fix_pub.publish(fix_msg)
angular_velocity = 0	
def cmd_vel_callback(msg):
	global angular_velocity
	angular_velocity = msg.angular.z;

def serial_connect():
	global serial_port
	try:
		serial_port = serial.Serial(rospy.get_param('serial_port', '/dev/ttyACM0'), rospy.get_param('baudrate', 115200), timeout=rospy.get_param('serial_timeout', 0.1)) #Initialize the serial port
		serial_port.write('a')
		serial_port.readline() #Clear out the buffer until the next newline so we receive a fully formed packet
		return True
	except serial.serialutil.SerialException, e:
		rospy.logerr("Serial error: " + str(e))
		return False
corrected_odom_position = [0, 0]
corrected_odom_position_set = False		
def corrected_odom_callback(msg):
	global corrected_odom_position, corrected_odom_position_set
	global X, Y
	corrected_odom_position[X] = msg.pose.position.x
	corrected_odom_position[Y] = msg.pose.position.y
	correct_odom_position_set = True

def dist(xa, ya, xb, yb):
	return math.sqrt((xa - xb) * (xa - xb) + (ya - yb) * (ya - yb))

def is_valid(pos):
	global X, Y, Z, T
	global corrected_odom_position, corrected_odom_position_set
	if not corrected_odom_position_set:
		return True
	distance = dist(pos[X], pos[Y], corrected_odom_position[X], corrected_odom_position[Y])
	return distance < rospy.get_param('position_delta_limit', 1.0)
odom_velocities = [0, 0, 0]
odom_velocities_set = False
def odom_callback(msg):
	global X, Y, Z
	odom_velocities[X] = msg.twist.twist.linear.x
	odom_velocities[Y] = msg.twist.twist.linear.y
	odom_velocities[Z] = msg.twist.twist.linear.z
	odom_velocities_set = True
	

r_to_d = 180.0/math.pi
d_to_r = math.pi/180.0	
def publisher():
	global calibrated_offset
	global rate
	global fix_pub	
	global calibrated_pub
	global d_to_r, r_to_d
	global serial_port
	global fix_cooldown, fix_cooldown_packet_count
	pose_pub = rospy.Publisher('/uwb/pose', PoseStamped, queue_size=10) #Initialize the pose publisher
	fix_pub = rospy.Publisher('/uwb/fix', Bool, queue_size=10) #Initialize the fix publisher
	calibrated_pub = rospy.Publisher('/uwb/last_calibration', UInt64, queue_size=10)
	cmd_vel_sub = rospy.Subscriber('/cmd_vel', Twist, cmd_vel_callback)
	corrected_odom_sub = rospy.Subscriber('/corrected_odom/pose', PoseStamped, corrected_odom_callback)
	odom_sub = rospy.Subscriber('/odom', Odometry, odom_callback)	
	rospy.init_node('uwb_interface_node', anonymous=False) #Initialize the node
	serial_connect()#Warning: does not reconnect at start and assumes that the serial port is available, otherwise will crash. 
	#The serial port options are all configurable by ROS params
	rate = rospy.Rate(rospy.get_param('rate', 50)) #Publish at 50 hz, unless otherwise set
	
	fix_cooldown_packet_count = rospy.get_param('fix_cooldown_packet_count', 20)
	rospy.loginfo("Node started.")
	while not rospy.is_shutdown():
		try: #Check for serial interruptions and handle them
			global serial_port
			serial_data = serial_port.readline() #Read the line of serial data
			#serial_port.flush()
		except serial.serialutil.SerialException:
			rospy.logerr("Unable to read from Serial port, likely interference.")
			serial_connect()
			do_loop(False)
			continue		
		if len(serial_data) != 41: #Each packet is always 41 characters long
			rospy.logerr("Invalid length of serial packet: %d.", len(serial_data)) #Invalid packet received	
			do_loop(False)		
			continue #Skip to the next iteration of the while loop	
		parsed_data = extract_serial_data(serial_data) #Parse the positions from the packet
		if parsed_data == False: #If false, the parsing was not successful
			do_loop(False)
			continue #Skip to the next iteration of the while loop	
		
		parsed_data[0] = parsed_data[0] / 1000.0
		parsed_data[1] = -parsed_data[1] / 1000.0 #Negate Y to align with standard coordinate conventions, see REP 103
		parsed_data[2] = parsed_data[2] / 1000.0
		parsed_data[3] = normalize_angle((parsed_data[3] * d_to_r) )#+ calibrated_offset)
		



		if has_fix(parsed_data):	
			calibrate_data(parsed_data)
			p = PoseStamped(); #Initialize the message
			p.header.stamp = rospy.Time.now() #Timestamp the pose
			p.header.frame_id = rospy.get_param('frame_id', 'map') #Set the coordinate frmae for the pose
			p.pose.position.x = parsed_data[0] #Push the position into the message and convert the millimeter measurements to meters
			p.pose.position.y = -parsed_data[1] 
			p.pose.position.z = parsed_data[2]
			
			theta = normalize_angle(parsed_data[3] + calibrated_offset)
			transform_theta = normalize_angle((math.pi / 2) - theta - (math.pi / 2))
			quat = euler_to_quaternion(transform_theta, 0, 0) #Convert the heading value to quaternions and offset to match REP 103
			p.pose.orientation.x = quat[0] #Push the quaternion into the message
			p.pose.orientation.y = quat[1]
			p.pose.orientation.z = quat[2]
			p.pose.orientation.w = quat[3]
			publish_fix(True)
			pose_pub.publish(p) #Publish the pose
			
			rospy.loginfo("Published pose: \nX %f, \nY %f, \nT %f. \nOffset: %f", p.pose.position.x, p.pose.position.y, transform_theta*r_to_d, calibrated_offset * r_to_d)
			rate.sleep()		
		else:
			if not is_valid(parsed_data):
				rospy.logwarn("UWB data not valid.")
			do_loop(False)


if __name__ == '__main__':
     try:
         publisher()
     except rospy.ROSInterruptException:
         pass
