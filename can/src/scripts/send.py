#!/usr/bin/env python
# coding: utf-8
##todo:count messages in 1 sec and print freq
##read and reciprocate (heartbeat), later put a timeout
##HB: recieve a message with id 11, check and send hb wit0x2
"""
This example shows how sending a single message works.
"""

#$ modprobe can_dev
#$ modprobe can
#$ modprobe can_raw
#  sudo ip link add dev vcan0 type vcan && sudo ip link set vcan0 type vcan && sudo ip link set up vcan0


# sudo ip link set can0 type can bitrate 500000
# sudo ip link set up can0

from __future__ import print_function
import can
import sys
import rospy

count = 0


def send_one():
#	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	
	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan
	MsgId = 0x11
	msg1 = can.Message(arbitration_id=MsgId,
	                  data=[0, 1, 2, 3, 4, 5, 6, 7],
	                  is_extended_id=False)
	                  
	msg12 = can.Message(arbitration_id=0x14,
	                  data=[0, 1, 2, 3, 4, 5, 6, 7],
	                  is_extended_id=False)
	                  
	try:
		bus.send(msg1)
		bus.send(msg12)
#		print("Message sent on {}".format(bus.channel_info))
		global count
		count += 1
		print(count)
	except can.CanError:
		print("Message NOT sent")

count = 1		
def send_two():
#	bus2 = can.interface.Bus(bustype='socketcan', channel='vcan1', bitrate=250000) #vcan
	
	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000)#can
	MsgId = 0x4F1
	global count
	msg2 = can.Message(arbitration_id=MsgId,
	                  data=[0, 0, 0, 0, 0, 0, 191, 0],
	                  is_extended_id=False)
	try:
		bus.send(msg2)
		print("Message sent on {}".format(bus.channel_info))
		
	#	if count < 4:
	#		count = count + 2
	#	else :
	#		count = 1
	except can.CanError:
		print("Message NOT sent")
    
	                  
if __name__ == "__main__":
	rospy.init_node('can_sender')
	r = rospy.Rate(20)
	while not rospy.is_shutdown():
		send_two()
		r.sleep()

    	

        
