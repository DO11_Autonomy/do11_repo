#!/usr/bin/env python
# coding: utf-8

"""""
Heartbeat: recieves and sends a known series of numbers to detect potential faults in communication.
Timeouts added if nothing recieved for a predetermined time.
"""""

import rospy
import can
import time
from time import sleep

count = 1
fault_count = 0
fault_threshold = 10

def send_beat(beat):
	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan
#	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	MsgId = 0x12
	msg_beat = can.Message(arbitration_id=MsgId,
	                  data=[beat, 1, 2, 3, 4, 5, 6, 7],
	                  is_extended_id=False)
	try:
		bus.send(msg_beat)
		print("Heartbeat Message sent on {}".format(bus.channel_info))
	except can.CanError:
		print("Heartbeat Message NOT sent")

def fault():
	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000)#vcan
#	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	MsgId = 0xF1
	
	msg_fault = can.Message(arbitration_id=MsgId,
	                  data=[255,],
	                  is_extended_id=False)
	try:
		bus.send(msg_fault)
		print("Fault Message sent on {}".format(bus.channel_info))
	except can.CanError:
		print("Fault Message NOT sent")


def check():
	bus = can.interface.Bus(bustype='socketcan', channel='vcan0', bitrate=500000) #vcan
#	bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=500000) #can
	start_time = time.time()
	global fault_count
	try:
		while fault_count < 30:
			msg = bus.recv(None)
			try:
				if msg.arbitration_id == 0x11: 
					time_now=time.time()
					diff_time = start_time - time_now
					global count
					if msg.data[0] == count:
						send_beat(count)
						if count < 4:
							count = count + 1
						else :
							count = 1
					else :
						
						fault_count = fault_count + 1
						print ("faults: ", fault_count)
						
			except AttributeError:
				print("Nothing received this time")
	except can.CanError:
		print("Message NOT sent")

if __name__ == '__main__':
	rospy.init_node("heart_beat")
	r=rospy.Rate(4)
	while not rospy.is_shutdown():
		if fault_count >= 30:
			print("fault")
			fault()
		else:
			print("check")
			check()
		r.sleep()

