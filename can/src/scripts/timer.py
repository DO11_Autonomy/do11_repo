#!/usr/bin/env python
# coding: utf-8
import rospy

class DemoNode():
  def __init__(self):
    print("entering constructor")
    #super().__init__('custom_talker')
    self.pub = rospy.Publisher('topic',customMessage, queue_size=10)
    self.counter = 1
    print("creating a publisher")
    self.timer = rospy.Timer(rospy.Duration(1), self.demo_callback)
    print("timer called")

  def demo_callback(self, timer):
    print("entering callback")
    msg = DetectedObject()
    print("Reading the message correctly")
    msg.id = self.counter
    self.counter += 1
    rospy.loginfo("Publishing message {}".format(msg.id))
    self.pub.publish(msg)

if __name__ == '__main__':

  print("entering main")
  rospy.init_node('custom_talker')
  try:
    DemoNode()
    print("entering Try")
    rospy.spin()
  except rospy.ROSInterruptException:
    print("exception thrown")
    pass
