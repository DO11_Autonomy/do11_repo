#!/usr/bin/env python

"""
CAN to ROS conversion of messages for showboat functions
Input from MAB via CAN
Output to ROS as a kickout to the do11_state machine
"""

import rospy
import can
import cantools
import sys
import numpy as np
from std_msgs.msg import Int32

# Load the dbc files and find the messages we need to send
db_ept = cantools.db.load_file('dbc_files/Nuevo_MAB.dbc')
mab_nuvo_msg = db_ept.get_message_by_name('Autonomy_Feedback')

# Setup the bus
bus2 = can.interface.Bus(bustype='socketcan',
                                channel='can0', bitrate=500000)

# Setup the publishers
pub_showboat_feedback = rospy.Publisher('ept_in/showboat_feedback', Int32, queue_size=10)
pub_sys_state = rospy.Publisher('ept_in/sys_state', Int32, queue_size=10)

def read_one():
    message = bus2.recv()
    dict = {}
#    dict = db_ept.decode_message(mab_nuvo_msg.arbitration_id, mab_nuvo_msg.data)
    dict = db_ept.decode_message(message.arbitration_id, message.data)
#    print (dict['Battery_Temp'])
    feedback_signal = Int32()
    feedback_signal.data = dict['SB_Feedback']
    sys_state_signal = Int32()
    sys_state_signal.data = dict['System_State']

    pub_showboat_feedback.publish(feedback_signal)
    pub_sys_state.publish(sys_state_signal)

if __name__ == '__main__':
    rospy.init_node('can_to_ros')
    r = rospy.Rate(20)
    while not rospy.is_shutdown():
        read_one()
        r.sleep()
