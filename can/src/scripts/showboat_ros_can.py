#!/usr/bin/env python

"""
ROS to CAN conversion of messages for showboat functions
Input from do11_state
Output to MAB via CAN
"""

import rospy
import can
import cantools
import sys
import numpy as np
from std_msgs.msg import Int32


# Load the dbc files and find the messages we need to send
db_ept = cantools.db.load_file('dbc_files/Nuevo_MAB.dbc')

showboat_msg = db_ept.get_message_by_name('Showboat')
state_msg = db_ept.get_message_by_name('Control_Commands')#TODO

#Setup the bus
bus = can.interface.Bus(bustype='socketcan',
                                channel='can0', bitrate=500000)

class showboat:
    def __init__(self):
        self.msg = Int32()
        self.sub_do11_state = rospy.Subscriber('do11_state', Int32, self.do11_state_callback)

    def send_msg(self, signals):
        MsgId = showboat_msg.frame_id
        #Convert numpy int64 to int
        data = showboat_msg.encode({ 'Five_Seat': signals[0].item(),
                                     'Six_Seat':  signals[1].item(),
                                     'Ramp_In':   signals[2].item(),
                                     'Ramp_Out':  signals[3].item(),
                                     'Door_Close':signals[4].item(),
                                     'Door_Open': signals[5].item(),
                                     'WC_Fold':   signals[6].item(),
                                     'WC_Unfold': signals[7].item()
                                     })
        msg_showboat = can.Message(arbitration_id=MsgId,
                                  data=data,
                                  is_extended_id=False)
        try:
            bus.send(msg_showboat)
            print("Showboat Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("Showboat Message NOT sent")

    def state_msg(self, state_value):
        MsgId = state_msg.frame_id
        #Convert numpy int64 to int
        data = state_msg.encode({ 'Required_Brake': 0,
                                  'Required_Steering': 0,
                                  'Required_Velocity': 0,
                                  'System_Mode':  state_value
                                })
        msg_state = can.Message(arbitration_id=MsgId,
                                  data=data,
                                  is_extended_id=False)
        try:
            bus.send(msg_state)
            print("Showboat Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("Showboat Message NOT sent")

    def process_info(self,state):
    # Vector for sending can msg, message as given below
    # [five seat, six seat, ramp in, ramp out, door close, door_open, wc fold, wc unfold]
        if state == 50: #showroom mode
            self.send_msg(np.array([0,0,0,0,0,0,0,0]))
        elif state == 51: #five seat
            self.send_msg(np.array([1,0,0,0,0,0,0,0]))
        elif state == 52: #six seat
            self.send_msg(np.array([0,1,0,0,0,0,0,0]))
        elif state == 53: #ramp in
            self.send_msg(np.array([0,0,1,0,0,0,0,0]))
        elif state == 54: #ramp out
            self.send_msg(np.array([0,0,0,1,0,0,0,0]))
        elif state == 55: #door close
            self.send_msg(np.array([0,0,0,0,1,0,0,0]))
        elif state == 56: #door open
            self.send_msg(np.array([0,0,0,0,0,1,0,0]))
        elif state == 57: #wheelchair fold
            self.send_msg(np.array([0,0,0,0,0,0,1,0]))
        elif state == 58: #wheelchair unfold
            self.send_msg(np.array([0,0,0,0,0,0,0,1]))

    def do11_state_callback(self, do11_state_data):
        rospy.loginfo("Receiving Information")  # Test the info inflow
        self.msg = do11_state_data  # 
        state = self.msg.data
        self.process_info(state)
        self.state_msg(state)

if __name__ == '__main__':
    rospy.init_node('ros_to_can')
    C = showboat()
    r = rospy.Rate(20)
    while not rospy.is_shutdown():
        rospy.spin()
