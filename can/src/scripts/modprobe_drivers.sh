#!/bin/bash
# Shell script to setup can0
# Only run this once the can convertor is connected

# Put these in bashrc
#sudo modprobe can
#sudo modprobe can_raw
#sudo modprobe can_dev

# Setup the can bus
#sudo ip link set can0 type can bitrate 500000
#sudo ip link set up can0
sudo ip link add dev vcan0 type vcan
sudo ip link set vcan0 type vcan
sudo ip link set up vcan0


#gnome-terminal --tab=1 -e roscore; sleep 3s

gnome-terminal --tab=2 -x roslaunch joy ps4joy.launch; sleep 3s
##os.system("gnome-terminal -x python joy_can_steer.py")
#gnome-terminal --tab=3 -e "python print.py" 

#gnome-terminal \
#	--tab -e roscore sleep 5s\
#	--tab -x roslaunch joy ps4joy.launch

#gnome-terminal \
#   --window 'Tab 1' \
#   --tab 'Tab2' -e roscore; sleep 5s\
#   --tab 'Tab3' -x roslaunch joy ps4joy.launch

#gnome-terminal --tab -e "roscore ; sleep 3s" --tab-with-profile=new -e "roslaunch joy ps4joy.launch" --tab-with-profile=new -- sh -c "python 'joy_can_steer.py' ; bash"

gnome-terminal --tab -- sh -c "python 'joy_can_steer.py' ; bash"


