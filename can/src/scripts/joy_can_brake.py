#!/usr/bin/env python

"""
Convert joy msgs to can for Kartech linear actuator
Used PEP8 style guide
"""

import rospy
import can
import cantools
import sys
from sensor_msgs.msg import Joy


db = cantools.db.load_file('dbc_files/brakebywire.dbc')
pos_msg = db.get_message_by_name('Position_Command_Default')

bus = can.interface.Bus(bustype='socketcan',
                                channel='can0', bitrate=250000)  # can

class joy_can_brake:
    def __init__(self):
        self.msg = Joy()
        self.brake_sub = rospy.Subscriber('/joy_orig', Joy, self.joy_callback)
    
    def send_brake(self, pressure):
        global pos_msg
        
        MsgId = pos_msg.frame_id
        data = pos_msg.encode({'message_type':15, 'confirmation_flag':0,
                                'auto_reply_flag':1, 'data_type':10,
                                'dpos':pressure, 'motor_enable':1, 'clutch_enable':1})
        
        msg_brake = can.Message(arbitration_id=MsgId,
                                  data=data,
                                  is_extended_id=True)
        try:
            bus.send(msg_brake)
            print("Braking Message sent on {}".format(bus.channel_info))
        except can.CanError:
            print("Steering Message NOT sent")


    def joy_callback(self, joy_data):
        rospy.loginfo("Receiving Information")  # Test the info inflow
        self.msg = joy_data  # initialize msg as joy data
        # Steering from xbox left joystick (1 = full left, -1 = full right)
        self.brake = self.msg.axes[5]
#        angle = int(133 - (self.steer * 90))
        self.brake = ((-self.brake) + 1)/2
        print(self.brake)
        pressure = int(550 + (self.brake * 2900))
        print(pressure)
        self.send_brake(pressure)
        rospy.loginfo("Publishing Information")  # Test the info inflow

    

if __name__ == '__main__':
    rospy.init_node('joy_can_kartech_brake')
    C = joy_can_brake()
    r = rospy.Rate(100)
    while not rospy.is_shutdown():
        rospy.spin()
