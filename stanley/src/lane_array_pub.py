#!/usr/bin/env python

import rospy 
from sensor_msgs.msg import Image,LaserScan,Joy
from geometry_msgs.msg import Pose,Twist
from autoware_msgs.msg import VehicleCmd, LaneArray

msg = LaneArray()
counter = 0

def Publisher():
    global msg
    rospy.loginfo("Sending Information")
    vehicle_pub = rospy.Publisher('/lane_waypoints_array1',LaneArray,queue_size=10)
    vehicle_pub.publish(msg)


def Subscriber():
    global counter
    if counter <= 1:
        rospy.Subscriber('/lane_waypoints_array',LaneArray,lane_callback)
     

def lane_callback(data):
    rospy.loginfo("Receiving Information")
    global counter
    counter += 1
    print(counter)
    global msg
    msg = data
    print(counter)
#    msg.axes = [0.0, data.ctrl_cmd.linear_velocity, 0.0, data.ctrl_cmd.steering_angle, 0.0, 0.0, 0.0]  




if __name__ == '__main__':
    rospy.init_node('converted')
    r = rospy.Rate(10)
    while not rospy.is_shutdown():
        if counter <= 1:
            Subscriber() 
        else:
            Publisher()
        r.sleep()
#        rospy.spin()
