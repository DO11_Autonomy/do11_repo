#ifndef PROJECT_CAMERA_MERGE_H
#define PROJECT_CAMERA_MERGE_H

#define __APP_NAME__ "camera_merge"

#include <string>
#include <vector>
#include <iostream>
#include <ctime>
#include <chrono>
#include <ros/ros.h>
#include <tf/tf.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/stitching.hpp"
#include <opencv2/core.hpp>
#include <opencv2/core/mat.hpp>

#include <sensor_msgs/point_cloud_conversion.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/transforms.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <Eigen/Eigen>

class ROSCameraMergeApp
{
  ros::NodeHandle                     node_handle_;
  ros::Publisher                      ground_plane_;
  ros::Publisher                      obstacles_;
  ros::Subscriber                     cloud_subscriber_;
  ros::Publisher                      image_pub_;
  // ros::Subscriber                     image_frt_lt_;
  // ros::Subscriber                     image_frt_rt_;
  // ros::Subscriber                     image_rt_;
  // ros::Subscriber                     image_lt_;
  // ros::Subscriber                     image_rr_;
  std::string                         input_cloud;

  bool 															  cloud_is_colored_;

  typedef
	message_filters::sync_policies::ApproximateTime<sensor_msgs::Image,
			sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::Image>   SyncPolicyT;

  message_filters::Subscriber<sensor_msgs::Image> *image_frt_lt_, *image_frt_rt_, *image_rt_, *image_lt_, *image_rr_;
  message_filters::Synchronizer<SyncPolicyT>              *image_synchronizer_;

  // message_filters::Subscriber<sensor_msgs::PointCloud2> cloud_subscriber_;

  void InitializeROSIo(ros::NodeHandle& in_private_handle);

  void ImageCallback(const sensor_msgs::Image::ConstPtr &in_frt_lt,
                     const sensor_msgs::Image::ConstPtr &in_frt_rt,
                     const sensor_msgs::Image::ConstPtr &in_lt,
                     const sensor_msgs::Image::ConstPtr &in_rt,
                     const sensor_msgs::Image::ConstPtr &in_rr);

  void PointsCallback(const sensor_msgs::PointCloud2::ConstPtr& parent_cloud_msg);

public:
  void Run();
  ROSCameraMergeApp();
};
#endif //PROJECT_CAMERA_MERGE_H
