#include "camera_merge/camera_merge.h"

void ROSCameraMergeApp::ImageCallback(const sensor_msgs::Image::ConstPtr &in_frt_lt,
                   const sensor_msgs::Image::ConstPtr &in_frt_rt,
                   const sensor_msgs::Image::ConstPtr &in_lt,
                   const sensor_msgs::Image::ConstPtr &in_rt,
                   const sensor_msgs::Image::ConstPtr &in_rr)
{
   cv_bridge::CvImagePtr cv_frt_lt = cv_bridge::toCvCopy(in_frt_lt, "bgr8");
   cv_bridge::CvImagePtr cv_frt_rt = cv_bridge::toCvCopy(in_frt_rt, "bgr8");
   cv_bridge::CvImagePtr cv_rt = cv_bridge::toCvCopy(in_rt, "bgr8");
   cv_bridge::CvImagePtr cv_lt = cv_bridge::toCvCopy(in_lt, "bgr8");
   cv_bridge::CvImagePtr cv_rr = cv_bridge::toCvCopy(in_rr, "bgr8");

   cv::Mat msg_frt_lt = cv_frt_lt->image;
   cv::Mat msg_frt_rt = cv_frt_rt->image;
   cv::Mat msg_lt = cv_lt->image;
   cv::Mat msg_rt = cv_rt->image;
   cv::Mat msg_rr = cv_rr->image;
   cv::Mat pano; // = cvCreateMat(msg_rr.rows, (msg_rr.cols)*5,3);
   cv::Mat pano_2;
   cv::Mat pano_3;

   msg_rr = msg_rr.colRange(150,msg_rr.cols-125);
   msg_frt_rt = msg_frt_rt.colRange(35,msg_frt_rt.cols);
   cv::hconcat(msg_lt, msg_frt_lt, pano);
   cv::hconcat(msg_frt_rt, msg_rt, pano_2);
   cv::hconcat(pano, pano_2, pano_3);
   cv::hconcat(msg_rr, pano_3, pano_3);

   // cv::imshow("OPENCV_WINDOW", pano_3);
   // cv::waitKey(1);

   cv_bridge::CvImagePtr out = cv_bridge::toCvCopy(in_rr, "bgr8");
   out->image = pano_3;
   out->header.frame_id = "fused_camera";
   out->header.stamp = ros::Time::now();

   image_pub_.publish(out->toImageMsg());
}

void ROSCameraMergeApp::InitializeROSIo(ros::NodeHandle &in_private_handle)
{
  std::string frt_lt, frt_rt, lt, rt, rr;
  std::string fused_lidar_str, ground_plane_cloud = "/ground_plane_cloud", obstacle_cloud = "/obstacle_cloud";
  std::string name_space_str = ros::this_node::getNamespace();

  if (name_space_str != "/")
	{
		fused_lidar_str = name_space_str + fused_lidar_str;
  }

  in_private_handle.param<std::string>("image_src", frt_lt, "camera_front_left/image_rectified");
	ROS_INFO("[%s] image_src: %s", __APP_NAME__, frt_lt.c_str());

  in_private_handle.param<std::string>("image_src", frt_rt, "camera_front_right/image_rectified");
	ROS_INFO("[%s] image_src: %s", __APP_NAME__, frt_rt.c_str());

  in_private_handle.param<std::string>("image_src", lt, "camera_left/image_rectified");
	ROS_INFO("[%s] image_src: %s", __APP_NAME__, lt.c_str());

  in_private_handle.param<std::string>("image_src", rt, "camera_right/image_rectified");
	ROS_INFO("[%s] image_src: %s", __APP_NAME__, rt.c_str());

  in_private_handle.param<std::string>("image_src", rr, "camera_rear/image_rectified");
	ROS_INFO("[%s] image_src: %s", __APP_NAME__, rr.c_str());

  // ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, frt_lt.c_str());
	// image_frt_lt_ = in_private_handle.subscribe(frt_lt,1,&ROSCameraMergeApp::ImageCallback, this);

  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, frt_lt.c_str());
  image_frt_lt_ = new message_filters::Subscriber<sensor_msgs::Image>(node_handle_, frt_lt, 10);

  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, frt_rt.c_str());
  image_frt_rt_ = new message_filters::Subscriber<sensor_msgs::Image>(node_handle_, frt_rt, 10);

  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, lt.c_str());
  image_lt_ = new message_filters::Subscriber<sensor_msgs::Image>(node_handle_, lt, 10);

  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, rt.c_str());
  image_rt_ = new message_filters::Subscriber<sensor_msgs::Image>(node_handle_, rt, 10);

  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, rr.c_str());
  image_rr_ = new message_filters::Subscriber<sensor_msgs::Image>(node_handle_, rr, 10);

  image_pub_ = node_handle_.advertise<sensor_msgs::Image>("/fused_image",1);
  ROS_INFO("[%s] Publishin to... %s", __APP_NAME__, "/fused_image");

  image_synchronizer_ =
			new message_filters::Synchronizer<SyncPolicyT>(SyncPolicyT(100),
																									 	 *image_frt_lt_,
																										 *image_frt_rt_,
																										 *image_lt_, *image_rt_, *image_rr_);

  image_synchronizer_->registerCallback(boost::bind(&ROSCameraMergeApp::ImageCallback, this, _1,_2,_3,_4,_5));

}

void ROSCameraMergeApp::Run()
{
  ros::NodeHandle private_node_handle("~");

  ROS_INFO("[%s] Ready. Waiting for data...", __APP_NAME__);

  InitializeROSIo(private_node_handle);

  ros::spin();

  ROS_INFO("[%s] END", __APP_NAME__);
}
ROSCameraMergeApp::ROSCameraMergeApp()
{
  cloud_is_colored_ = false;
}
