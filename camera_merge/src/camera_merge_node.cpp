#include "camera_merge/camera_merge.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, __APP_NAME__);

  ROSCameraMergeApp app;

  app.Run();

  return 0;
}
