#include <iostream>
#include <vector>
#include <ros/ros.h>
#include <string>
#include <std_msgs/Int32.h> 
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>
#include "tf2_msgs/TFMessage.h"
#include <autoware_msgs/VehicleCmd.h>
#include <autoware_msgs/ControlCommand.h>
#include <sensor_msgs/Joy.h>

using namespace std;


class TwistGate{
  public:
    TwistGate();
    ~TwistGate();

  private:
    ros::NodeHandle nh_;
  
    // Add subscribers here
    ros::Subscriber do11_state_sub_;
    ros::Subscriber joy_sub_;
    ros::Subscriber stanley_sub_;  
    
    // Add publishers here
    ros::Publisher final_vehicle_cmd_;    
    
    /*ROS callbacks*/
    void do11StateCallback(const std_msgs::Int32::ConstPtr& msg);
    void joyCallback(const sensor_msgs::Joy::ConstPtr& msg);
    void stanleyCallback(const autoware_msgs::VehicleCmd::ConstPtr& msg);
    
    void switchState(const ros::TimerEvent&);
    void publishFinalVehicleCmd(const float steer, const float velocity, const float brake);

//    // Add private member variables here
    float joy_steer_{0};
    float joy_velocity_{0};
    float joy_brake_{0};
    
    float stanley_steer_{0};
    float stanley_velocity_{0};

//    // Add private member function declarations here

//    /*shared pointers*/
    std::shared_ptr <std_msgs::Int32> current_state_;
    std::shared_ptr <sensor_msgs::Joy> current_joy_;
    std::shared_ptr <autoware_msgs::VehicleCmd> current_stanley_;
    
    ros::Timer timer1;


};
