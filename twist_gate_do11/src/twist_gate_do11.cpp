#include "twist_gate_do11/twist_gate_do11.h"

TwistGate::TwistGate() : nh_("")
{

  do11_state_sub_ =  nh_.subscribe("do11_state", 1000, &TwistGate::do11StateCallback,this);
  joy_sub_ =  nh_.subscribe("joy_orig", 1000, &TwistGate::joyCallback, this);
  stanley_sub_ = nh_.subscribe("vehicle_cmd", 1000, &TwistGate::stanleyCallback, this); 
  
  
  final_vehicle_cmd_ = nh_.advertise<autoware_msgs::ControlCommand>("final_vehicle_cmd", 10);


  timer1 = nh_.createTimer(ros::Duration(0.1), &TwistGate::switchState, this);
};

TwistGate::~TwistGate(){};



void TwistGate::do11StateCallback(const std_msgs::Int32::ConstPtr& msg)
{
  current_state_ = std::make_shared<std_msgs::Int32>(*msg);  
}

void TwistGate::joyCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
  current_joy_ = std::make_shared<sensor_msgs::Joy>(*msg);
  joy_steer_ = current_joy_->axes[3];
  joy_velocity_ = current_joy_->axes[1];
  joy_brake_ = current_joy_->axes[5];
}

void TwistGate::stanleyCallback( const autoware_msgs::VehicleCmd::ConstPtr& msg)
{
  current_stanley_ = std::make_shared<autoware_msgs::VehicleCmd>(*msg);
  stanley_velocity_ = current_stanley_->ctrl_cmd.linear_velocity;
  stanley_steer_ = current_stanley_->ctrl_cmd.steering_angle;
}

void TwistGate::switchState(const ros::TimerEvent&)
{
  if(current_state_ != nullptr)
  {

   switch(current_state_->data) {
      case 20 :
        cout << "Default\n";
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      // Nominal mode states--------------------------------
      case 0 :
        cout << "Nominal: V_Prop OK";
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 1 :
        cout << "Nominal: Steering Enabled";
        publishFinalVehicleCmd(joy_steer_, 0.0, joy_brake_);
        break;
      case 2 :
        cout << "Nominal: Velocity & Steering Enabled";
        publishFinalVehicleCmd(joy_steer_, joy_velocity_, joy_brake_);
        break;
      case 3 :
        cout << "Nominal: AUTONOMOUS";
        //1.0 brake is no brake 
        publishFinalVehicleCmd(stanley_steer_, stanley_velocity_, 1.0);
        break;
      // Emergency mode-------------------------------------
      case 100 :
        cout << "EMERGENCY!!";
        //-1.0 brake is full brake 
        publishFinalVehicleCmd(0.0, 0.0, 1.0);
        break;
      // Showroom mode states-------------------------------
      case 50 :
        cout << "State : Showroom Default" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 51 :
        cout << "Showroom : Five Seat configuration" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 52 :
        cout << "Showroom : Six Seat configuration" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 53 :
        cout << "Showroom : Ramp closing" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 54 :
        cout << "Showroom : Ramp opening" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 55 :
        cout << "Showroom : Door closing" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 56 :
        cout << "Showroom : Door opening" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 57 :
        cout << "Showroom : Wheelchair folding" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 58 :
        cout << "Showroom : Wheelchair unfolding" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 70 :
        cout << "Showroom : Ramp Open" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 71 :
        cout << "Showroom : Door Open" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      case 72 :
        cout << "Showroom : Wheelchair unfolded" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
        break;
      default :
        cout << "Invalid state" << endl;
        publishFinalVehicleCmd(0.0, 0.0, 0.0);
   }
  }
}

void TwistGate::publishFinalVehicleCmd(float steer, float velocity, float brake)
{
  cout<< "   steer = " << steer << " velocity = " << velocity << " brake = " << brake << endl;
  autoware_msgs::ControlCommand final_cmd;
  final_cmd.steering_angle = steer;
  final_cmd.linear_velocity = velocity;
  //USING LINEAR ACCELERATION SLOT AS BRAKE SINCE IT IS A FLOAT TYPE
  final_cmd.linear_acceleration = brake;
  final_vehicle_cmd_.publish(final_cmd);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "twist_gate_do11");
  TwistGate obj;
  //Using AsyncSpinner for asyncronous threading
  ros::AsyncSpinner spinner(0);
  spinner.start();
  ros::waitForShutdown();
  return 0;
};
