import cv2
import math
import rospy
import numpy as np
from nav_msgs.msg import OccupancyGrid,GridCells
from geometry_msgs.msg import Point
import sensor_msgs.point_cloud2 as pc2 
from visualization_msgs.msg import MarkerArray,Marker
from sensor_msgs.msg import PointCloud2,Image,LaserScan
from ukf import UKF
from autoware_msgs.msg import PointsImage,DetectedObjectArray,Centroids,CloudClusterArray
from threading import Lock


class Tracker(object):   

    def __init__(self):
        self.q = np.eye(6)
        self.q[0][0] = 0.0001
        self.q[1][1] = 0.0001
        self.q[2][2] = 0.0004
        self.q[3][3] = 0.0025
        self.q[4][4] = 0.0025
        self.q[5][5] = 0.0025
        self.r_lidar = np.zeros([2,2])
        self.r_lidar[0][0] = 0.15
        self.r_lidar[1][1] = 0.15 
        self.lidar_sub = rospy.Subscriber('/cluster_centroids',Centroids,self.lidar_tracker)
        self.state_estimator = UKF(6, self.q, np.zeros(6), 0.0001*np.eye(6), 0.04, 0.0, 2.0, self.iterate_x)

    
    def iterate_x(self,x_in, timestep, inputs):
        '''this function is based on the x_dot and can be nonlinear as needed'''
        ret = np.zeros(len(x_in))
        ret[0] = x_in[0] + timestep * x_in[3] * math.cos(x_in[2])
        ret[1] = x_in[1] + timestep * x_in[3] * math.sin(x_in[2])
        ret[2] = x_in[2] + timestep * x_in[4]
        ret[3] = x_in[3] + timestep * x_in[5]
        ret[4] = x_in[4]
        ret[5] = x_in[5]
        return ret

    

    def lidar_tracker(self,data):
        marker_pub = rospy.Publisher('/tracked_object',Marker,queue_size=10)
        
            #current_time = rospy.get_time()
        dt = 0.1
        lidar_data = np.array([data.points[0].x,data.points[0].y])
        self.state_estimator.predict(dt)
        self.state_estimator.update([0,1],lidar_data,self.r_lidar)
        est_state = self.state_estimator.get_state()
        rospy.loginfo(est_state)
        msg = Marker()
        msg.header.frame_id = 'velodyne'
        msg.header.stamp = rospy.Time()
        msg.type = msg.SPHERE
        msg.ns = "tracked_objects"
        msg.id = 1
        msg.action = msg.ADD
        msg.pose.position.x = est_state[0]
        msg.pose.position.y = est_state[1]
        msg.pose.position.z = data.points[1].z
        msg.pose.orientation.x = 0.0
        msg.pose.orientation.y = 0.0
        msg.pose.orientation.z = 0.0
        msg.pose.orientation.w = 1.0
        msg.scale.x = 0.5
        msg.scale.y = 0.5
        msg.scale.z = 0.5
        msg.color.a = 1.0 
        msg.color.r = 1.0
        msg.color.g = 0.0
        msg.color.b = 0.0
        marker_pub.publish(msg)
        
       
        # first define time interval 
        # then define the 



if __name__ =='__main__':
    Tracker()
    rospy.init_node('Tracker',anonymous=True)
    rospy.loginfo('starting node')
    rospy.Rate(10)
    rospy.spin()   
    
        

        



        
        
        









    
      

      
       
         

     






    





    

    