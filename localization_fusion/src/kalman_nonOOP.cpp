#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "geometry_msgs/PoseStamped.h"
#include <autoware_msgs/NDTStat.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>

ros::Publisher pub;
float x_final;
float y_final;

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void ndtCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
//  ROS_INFO("NDT_X: [%f]", msg->pose.position.x);
//  ROS_INFO("NDT_Y: [%f]", msg->pose.position.y);
//  ROS_INFO("NDT_Z: [%f]", msg->pose.position.z);
  x_final = msg->pose.position.x;
  y_final = msg->pose.position.y;
  geometry_msgs::PoseStamped new_msg;
  new_msg.pose.position.x = x_final;
  new_msg.pose.position.y = y_final;
  pub.publish(new_msg);
}
void ndtCheckCallback(const autoware_msgs::NDTStat::ConstPtr& msg)
{
  ROS_INFO("NDT_Check: [%f]", msg->score);
}
void uwbCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  ROS_INFO("UWB_X: [%f]", msg->pose.position.x);
  ROS_INFO("UWB_Y: [%f]", msg->pose.position.y);
  ROS_INFO("UWB_Z: [%f]", msg->pose.position.z);
}
void uwbCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
  ROS_INFO("UWB_Check: [%s]", msg->data ? "True" : "False");
}
void gnssCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  ROS_INFO("GNSS_X: [%f]", msg->pose.pose.position.x);
  ROS_INFO("GNSS_Y: [%f]", msg->pose.pose.position.y);
  ROS_INFO("GNSS_Z: [%f]", msg->pose.pose.position.z);
  //Could use orientation here as well from same topic and also long lat altitude for global frame
  //from /gps/fix    
  
}
void gnssCheckCallback(const sensor_msgs::NavSatFix::ConstPtr& msg)
{
    //NOT SURE IF THIS WORKS. If RTK fix is not available does it publish anything
  ROS_INFO("GNSS_Check: [%d]", msg->status.status);
  //If Piksi is in Fixed Mode (has an rtk fix), variance is populated with the reported rtk horizontal accuracy; otherwise it is populated with the value 1000. 
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");

  ros::NodeHandle n;

  ros::Subscriber sub_ndt = n.subscribe("ndt_pose", 1000, ndtCallback);
//  ros::Subscriber sub_ndt_check = n.subscribe("ndt_stat", 1000, ndtCheckCallback);
//  ros::Subscriber sub_uwb = n.subscribe("uwb/pose", 1000, uwbCallback);
//  ros::Subscriber sub_uwb_check = n.subscribe("uwb/fix", 1000, uwbCheckCallback);
//  ros::Subscriber sub_gnss = n.subscribe("gps/rtkfix", 1000, gnssCallback);
//  ros::Subscriber sub_gnss_check = n.subscribe("gps/fix", 1000, gnssCheckCallback);
  pub = n.advertise<geometry_msgs::PoseStamped>("/kalman_pose", 1);


  ros::spin();

  return 0;
}
