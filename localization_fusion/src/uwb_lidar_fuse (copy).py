#!/usr/bin/env python
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import cv2
import rospy 

from sensor_msgs.msg import Image,LaserScan
from geometry_msgs.msg import Pose,Point
import sys

class obstacledetection:

    def __init__(self):
        self.msg = LaserScan()
        self.scan_sub=rospy.Subscriber('/scan',LaserScan,self.scan_callback)
        self.scan_pub=rospy.Publisher('Scan',LaserScan,queue_size=10)
        self.horizon=2 # meters
        self.count=0
        self.cone_start=-90
        self.cone_end=90
        self.counter=0 #counter for datapoints
        # self.f=open("dataXY.txt","a+")
        ## initialize gloabal variables for the kalman filter
        self.spInv=np.matrix([[1, 0, dT, 0],[0, 1, 0, dT],[0, 0, 1, 0],[0, 0, 0, 1]])
        self.spInvt=np.transpose(spInv)
        self.St=np.matrix([[1,0,0.2,0],[0,1,0,0.2],[0,0,1,0],[0,0,0,1]])
        self.Q=np.matrix([[0,0,0,0],[0,0,0,0],[0,0,0.001,0],[0,0,0,0.001]]) #process noise
        self.M=np.matrix([[1,0,0,0],[0,1,0,0]])
        self.Mt=np.transpose(M)
        self.R=np.matrix([[0.9,0],[0,0.9]]) #measurement noise-default 0.1 diagonal 
        self.X=np.matrix([[x[14]],[y[14]],[0],[0]])
        ## initialize the publisherso
        self.pub=rospy.Publisher('/obstacle_pos',Point,queue_size=1)
        self.pub1=rospy.Publisher('/obstacle_pos_ini',Point,queue_size=1)


    def scan_callback(self,scan_data):
        #rospy.loginfo("Receiving Information")# Test the info inflow
        self.msg=scan_data # initialize msg as the scan data
        self.scan_pub.publish(self.msg)# publish not used ATM
        self.msg.angle_max = round(self.msg.angle_max,5) # Rounded to 5 decimal points
        self.msg.angle_min=round(self.msg.angle_min,5) # Rounded to 5 decimal points
        self.msg.angle_increment=round(self.msg.angle_increment,5) # Rounded to 5 decimal points
        self.steps = int((self.msg.angle_max - self.msg.angle_min)/(self.msg.angle_increment)) # No. of sampling steps in the desired cone
        self.steps=self.steps+1
        self.angles=list([])#initialize empty LIST to leverage the index function
        self.cone_index=list()
        #Limit the cone range 
        for i in range(0,self.steps):
            a = self.msg.angle_min+i*self.msg.angle_increment
            self.angles.append(a)
            if a > 0.0175*self.cone_start and a < 0.0175*self.cone_end:# converting to radians
                self.cone_index.append(i)
        self.msg.angle_max=self.msg.angle_min+(self.steps-1)*self.msg.angle_increment #calc max angle in rad
        self.start_index = self.cone_index[0]
        self.end_index=self.cone_index[-1]
        self.obstaclelist = self.obstacledetector() #create and return obstacle list
        self.obstaclelist = self.boundary_generator() #create bounding boxes

    def obstacledetector(self):

        count = 0
        self.obstaclelist = list()
        self.obstacle = list()
        for i in range(self.start_index,self.end_index):
            if self.msg.ranges[i] < self.horizon and self.msg.ranges[i] > 0:
                a = abs(self.msg.ranges[i]-self.msg.ranges[i+1])
                if a <= 0.1: # meter tolerance to classify scan data that belongs to one obstacle
                    self.obstacle.append(i)
                else:
                    self.obstaclelist.append(self.obstacle)
                    self.obstacle = list()
        self.obstacle_number = len(self.obstaclelist)
        print('no of obs= ', self.obstacle_number)
        return self.obstaclelist
    
    def boundary_generator(self):
        plt.close("all")
        fig = plt.figure()
        ax = plt.Axes(fig,[0.,0.,1.,1.])
        self.blank_image = np.zeros(shape=[512,512,3],dtype=np.uint8)

        for i in range(len(self.obstaclelist)):
            x=[]
            y=[]
            max = 0
            min = 100
            for j in range(len(self.obstaclelist[i])):
                if self.msg.ranges[self.obstaclelist[i][j]] < min:
                    min = self.msg.ranges[self.obstaclelist[i][j]]
                    min_rangeindex = j
                if self.msg.ranges[self.obstaclelist[i][j]] > max:
                    max = self.msg.ranges[self.obstaclelist[i][j]]
                    max_rangeindex = j
            left_index = self.obstaclelist[i][0]
            right_index = self.obstaclelist[i][-1]
            if self.angles[min_rangeindex] < 0:
                x_min = -self.msg.ranges[min_rangeindex]*np.sin(abs(self.angles[min_rangeindex]))
                y_min = self.msg.ranges[min_rangeindex]*np.cos(abs(self.angles[min_rangeindex]))
            else:
                x_min = self.msg.ranges[min_rangeindex]*np.sin(abs(self.angles[min_rangeindex]))
                y_min = self.msg.ranges[min_rangeindex]*np.cos(abs(self.angles[min_rangeindex]))
            
            if self.angles[left_index] < 0:
                x_left = -self.msg.ranges[left_index]*np.sin(abs(self.angles[left_index]))
                y_left = self.msg.ranges[left_index]*np.cos(abs(self.angles[left_index]))
            else:
                x_left = self.msg.ranges[left_index]*np.sin(abs(self.angles[left_index]))
                y_left = self.msg.ranges[left_index]*np.cos(abs(self.angles[left_index]))
            if self.angles[right_index] < 0:
                x_right = -self.msg.ranges[right_index]*np.sin(abs(self.angles[right_index]))
                y_right = self.msg.ranges[right_index]*np.cos(abs(self.angles[right_index]))
            else:
                x_right = self.msg.ranges[right_index]*np.sin(abs(self.angles[right_index]))
                y_right = self.msg.ranges[right_index]*np.cos(abs(self.angles[right_index]))
            Numerator = 2*abs((y_right-y_left)*x_min-(x_right-x_left)*y_min+x_right*y_left-y_right*x_left) 
            Denominator = np.sqrt(np.square(y_right-y_left)+np.square(x_right-x_left))
            Width = Numerator/Denominator
            Height = Denominator
            print(x_left,y_left)
            print(x_right,y_right)
            
            x_center = (x_right+x_left)/2
            y_center = (y_right+y_left)/2
            filtered_states=kalman(x_center,y_center)
            x_center=filtered_states[0]
            y_center=filtered_states[1]
            if self.count<1:
                self.ini_x=x_center
                self.ini_y=y_center
            self.count=self.count+1
            self.obstacle_pos_ini = Point()
            self.obstacle_pos_ini.x=self.ini_x
            self.obstacle_pos_ini.y=self.ini_y
            self.obstacle_pos_ini.z=0
            self.pub1.publish(self.obstacle_pos_ini)
            rospy.loginfo("Publishing Information")# Test the info inflow
            self.obstacle_pos = Point()
            self.obstacle_pos.x=x_center
            self.obstacle_pos.y=y_center
            self.obstacle_pos.z=0
            self.pub.publish(self.obstacle_pos)
            rospy.loginfo("Publishing Information")# Test the info outflow
            self.f.write("%f %f\n"%(x_center,y_center))
            y_right = y_right*100 + 256
            y_left = y_left*100 + 256
            x_right = x_right*100 + 256
            x_left = x_left*100 + 256
            x_min = x_min*100 + 256
            y_min = y_min*100 +256
            m = (y_right-y_left)/(x_right-x_left)
            y_left_bottom = ((y_min+(m**2)*y_left+(m)*(x_left-x_min))/(1+(m**2)))
            x_left_bottom = (y_left-y_left_bottom)*m + x_left 
            #xy_left_bottom = (int(x_left_bottom),int(y_left_bottom))
            rotation_angle = -1/m*180/np.pi
            xy_end_point = (int(x_right),int(y_right))
            # print(xy_left_bottom,xy_end_point)
            cv2.rectangle(self.blank_image,xy_left_bottom,xy_end_point,(255,255,255),-1)
        cv2.rectangle(self.blank_image,(250,250),(256,256),(255,0,0),-1)
        cv2.imshow("blank image",self.blank_image)  
        cv2.waitKey(1)


    def Kalman_filter(self, x,y):
        dT=1    
        Y=np.matrix([[0],[0]])
        I=np.identity(4)
        x_filt=np.zeros(len(x))
        y_filt=np.zeros(len(y))
        #Filtering equations
        self.X=self.spInv*self.X # state transition
        self.St=self.spInv*self.St*self.spInvt + self.Q # State covariance prediction
        self.K=self.St*self.Mt*inv(self.M*self.St*self.Mt + self.R)# Kalman gain
        Y=np.matrix([[x],[y]])# measurement step
        self.X=self.X + self.K*(Y - self.M*self.X)# update states based on kalman gain
        x_filt=self.X[0] 
        y_filt=self.X[1]
        St=(I - K*M)*St# update the state covariances
        f.close()
        return x_filt,y_filt

if __name__ == '__main__':
    rospy.init_node('obstacledetection')
    C = obstacledetection()  
    r = rospy.Rate(40)
    if rospy.is_shutdown():
        self.f.close()
    while not rospy.is_shutdown():
        r.sleep()
