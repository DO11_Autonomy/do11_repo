#include "localization_fusion/kalman.h"


KalmanFilter::KalmanFilter() : nh_("")
{
  filtered_pub_ =  nh_.advertise<geometry_msgs::PoseStamped>("/kalman_pose", 1);
  uwb_test_pub_ = nh_.advertise<std_msgs::Bool>("/testuwbcheck",1);
  ndt_sub_ =  nh_.subscribe("ndt_pose", 1000, &KalmanFilter::ndtCallback,this);
  ndt_check_sub_ =  nh_.subscribe("ndt_stat", 1000, &KalmanFilter::ndtCheckCallback, this);
  uwb_sub_ =  nh_.subscribe("uwb/pose", 1000, &KalmanFilter::uwbCallback, this);
  uwb_check_sub_ =  nh_.subscribe("uwb/fix", 1000, &KalmanFilter::uwbCheckCallback, this);
  gnss_sub_ =  nh_.subscribe("gps/rtkfix", 1000, &KalmanFilter::gnssCallback, this);
  gnss_check_sub_ =  nh_.subscribe("gps/fix", 1000, &KalmanFilter::gnssCheckCallback, this);
  St << 1,0,1,0,
           0,1,0,1,
           5,0,1,0,
           0,0,0,1;
  X << 14,14,0,0;

};

KalmanFilter::~KalmanFilter(){};



int main(int argc, char **argv)
{

  ros::init(argc, argv, "kf_localizer");
  KalmanFilter obj;
  
  ros::AsyncSpinner spinner(0);
  spinner.start();

  ros::waitForShutdown();

  return 0;
};


void KalmanFilter::checkNumberofPoses()
{
  ros::Time current_time = ros::Time::now();

  int use_ndt = 0;
  int use_uwb = 0;
  int use_gnss = 0;
//  ROS_INFO("NDT_Check: [%f]", current_ndt_stat_->score);
//  ROS_INFO("NDT_pose: [%f]", current_ndt_pose_->pose.position.x);
//  if(current_ndt_stat_->score <= 100.0)
//  {
//    use_ndt = 1;
//    kalmanUpdate(current_ndt_pose_->pose.position.x,current_ndt_pose_->pose.position.y);
//  }
//  else
//    use_ndt = 0;
    
//  if(current_uwb_stat_->data){ //comparing an already boolean value
//    use_uwb = 1;
//    measurementUWBUpdatePose(*current_uwb_pose_);
//  }
//  else
//    use_uwb = 0;
//    
  if(current_gnss_stat_->status.status == 0){
    use_gnss = 1;
    kalmanUpdate(current_gnss_pose_->pose.pose.position.x,current_gnss_pose_->pose.pose.position.y);
  }
  else
    use_gnss = 0;
    
  //ROS_INFO("NDT = [%d], UWB =  [%d], GNSS =  [%d]", use_ndt, use_uwb, use_gnss);
}




/** These are the simple callbacks to get information from the corresponding topics of NDT, UWB and GNSS **/

void KalmanFilter::ndtCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  current_ndt_pose_ = std::make_shared<geometry_msgs::PoseStamped>(*msg);
  //checkNumberofPoses();
  
}

void KalmanFilter::ndtCheckCallback(const autoware_msgs::NDTStat::ConstPtr& msg)
{
  current_ndt_stat_ = std::make_shared<autoware_msgs::NDTStat>(*msg);
  //checkNumberofPoses();
//  ROS_INFO("NDT_Check: [%f]", msg->score);
//  if(current_ndt_stat_->score <= 100.0)
//  {
//    use_ndt = 1;
//    kalmanUpdate(current_ndt_pose_->pose.position.x,current_ndt_pose_->pose.position.y);
//  }
//  else
//    use_ndt = 0;
}
void KalmanFilter::uwbCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  current_uwb_pose_ = std::make_shared<geometry_msgs::PoseStamped>(*msg);
}

void KalmanFilter::uwbCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
  
  current_uwb_stat_ = std::make_shared<std_msgs::Bool>(*msg);
//  if(current_uwb_stat_->data)
//  {
//    use_uwb = 1;
//    kalmanUpdate(current_uwb_pose_->pose.position.x,current_uwb_pose_->pose.position.y);
//  }
//  else
//    use_uwb = 0;
    
//  if(msg->data)
//    ROS_INFO("yes");
//  else
//    ROS_INFO("no");
  //current_uwb_stat_ = (msg->data) ? "True" : "False";
//  ROS_INFO("UWB_Check: [%s]", msg->data ? "True" : "False");
  //checkNumberofPoses();
}
void KalmanFilter::gnssCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
//  ROS_INFO("GNSS_X: [%f]", msg->pose.pose.position.x);
//  ROS_INFO("GNSS_Y: [%f]", msg->pose.pose.position.y);
//  ROS_INFO("GNSS_Z: [%f]", msg->pose.pose.position.z);
  //Could use orientation here as well from same topic and also long lat altitude for global frame
  //from /gps/fix    
  current_gnss_pose_ = std::make_shared <nav_msgs::Odometry>(*msg);
  ROS_INFO("GNSS_X: [%f]", msg->pose.pose.position.x);
  ROS_INFO("GNSS_Y: [%f]", msg->pose.pose.position.y);
  //kalmanUpdate(msg->pose.pose.position.x,msg->pose.pose.position.y);
  //checkNumberofPoses();
  
}
void KalmanFilter::gnssCheckCallback(const sensor_msgs::NavSatFix::ConstPtr& msg)
{
  current_gnss_stat_ = std::make_shared<sensor_msgs::NavSatFix>(*msg);
  ROS_INFO("GNSS_Check: [%d]", msg->status.status);
  if(current_gnss_pose_)
  {
    ROS_INFO("GNSS_X: [%f]", current_gnss_pose_->pose.pose.position.x);
    ROS_INFO("GNSS_Y: [%f]", current_gnss_pose_->pose.pose.position.y);
    checkNumberofPoses();
  }
//  if(current_gnss_stat_->status.status == 0)
//  {
//    use_gnss = 1;
//    kalmanUpdate(current_gnss_pose_->pose.pose.position.x,current_gnss_pose_->pose.pose.position.y);
//  }
//  else
//    use_gnss = 0;

    //NOT SURE IF THIS WORKS. If RTK fix is not available does it publish anything
//  ROS_INFO("GNSS_Check: [%d]", msg->status.status);
  //If Piksi is in Fixed Mode (has an rtk fix), variance is populated with the reported rtk horizontal accuracy; otherwise it is populated with the value 1000. 
}


void KalmanFilter::kalmanUpdate(float x,float y)
{
  ROS_INFO("In Kalmanupdate");
  std::cout << x << y << "\n\n";
  float dT = 0.1;
  Eigen::MatrixXf Y = Eigen::MatrixXf::Zero(2, 1);
  Eigen::MatrixXf I = Eigen::MatrixXf::Identity(2, 2);
  Eigen::Matrix4f spInv,spInvt;
  spInv << 1.0,0.01,dT,0.01,
           0.01,1.0,0.01,dT,
           0.01,0.01,1.0,0.01,
           0.01,0.01,0.01,1.0;
  spInvt = spInv.transpose();
  Eigen::Matrix4f Q;
  Q << 0,0,0,0,
       0,0,0,0,
       0,0,0.001,0,
       0,0,0,0.001;
  Eigen::MatrixXf M = Eigen::MatrixXf::Zero(2,4);
  Eigen::MatrixXf Mt = Eigen::MatrixXf::Zero(4,2);
  M << 1,0,0,0,
       0,1,0,0;
  Mt = M.transpose();
  Eigen::MatrixXf R = Eigen::MatrixXf::Zero(2,2);
  R << 0.9,0,
       0,0.9;
  
  X = spInv*X;

  St = (spInv*St*spInvt)+Q;
  std::cout << "St" << "\n";
  std::cout << St << "\n\n";
  Ktemp = (M*St*Mt)+R;
  std::cout << "Ktemp" << "\n";
  std::cout << Ktemp << "\n\n";
  K = Ktemp.inverse();
  std::cout << "K" << "\n";
  std::cout << K << "\n\n";
  K = St*Mt*K;
  std::cout << "K" << "\n";
  std::cout << K << "\n\n";


  
  Y(0,0) = x;
  Y(1,0) = y;
  
  X = X + K*(Y - M*X);
  x_filt = X(0);

  y_filt = X(1);
  //std::cout << x_filt << y_filt << "\n\n";
  
  St = (I-(K*M))*St;
  std::cout << "St" << "\n";
  std::cout << St << "\n\n";
  
  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = "world";
  pose.pose.position.x = x_filt;
  pose.pose.position.y = y_filt;
  pose.pose.position.z = 0.0;

  pose.pose.orientation.x = 0.0;
  pose.pose.orientation.y = 0.0;
  pose.pose.orientation.z = 0.0;
  pose.pose.orientation.w = 0.0;
  filtered_pub_.publish(pose);
//  std::cout << St << "\n\n";
//  std::cout << I << "\n\n";
//  std::cout << K << "\n\n";
//  std::cout << M << "\n\n";
//  St(1,0) = St(1,0) + 4;
//  std::cout << St << "\n";
  
}




/**These are the functions that update the filter based on the reliable measurments received**/
void KalmanFilter::measurementNDTUpdatePose()
{
//  kalmanUpdate(current_gnss_pose_->pose.pose.position.x,current_gnss_pose_->pose.pose.position.y);
//  kalmanUpdate(current_uwb_pose_->pose.position.x,current_uwb_pose_->pose.position.y);
//  geometry_msgs::PoseStamped pose;
//  pose.header.frame_id = "world";
//  pose.pose.position.x = x_filt;
//  pose.pose.position.y = y_filt;
//  pose.pose.position.z = 0.0;

//  pose.pose.orientation.x = 0.0;
//  pose.pose.orientation.y = 0.0;
//  pose.pose.orientation.z = 0.0;
//  pose.pose.orientation.w = 0.0;
//  filtered_pub_.publish(pose);

}

void KalmanFilter::measurementUWBUpdatePose(const geometry_msgs::PoseStamped &pose)
{
  //filtered_pub_.publish(pose);
}

void KalmanFilter::measurementGNSSUpdatePose(const nav_msgs::Odometry &pose)
{

}



