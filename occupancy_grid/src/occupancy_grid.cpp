#include "occupancy_grid/occupancy_grid.h"

OccupancyGrid::OccupancyGrid() : nh_(""), pnh_("~")
{
  // Add rosparamters here
  pnh_.param("laser_scan_topic", laser_scan_topic_, std::string("/scan"));
  pnh_.param("vector_map_topic", vector_map_topic_, std::string("/vector_map"));
  pnh_.param("fused_lidar_topic", fused_lidar_topic_, std::string("/fused_cropped_lidar"));
  pnh_.param("occupancy_frame", occupancy_frame_, std::string("/base_link"));
  pnh_.param("vector_map_frame", vector_map_frame_, std::string("/map"));
  pnh_.param("map_width", map_width_, int(56));
  pnh_.param("map_height", map_height_, int(56));
  pnh_.param("resolution", resolution_, double(0.5));
  pnh_.param("stop_box_x_max", stop_box_x_max_, double(6.0));
  pnh_.param("stop_box_y_max", stop_box_y_min_, double(1.2));
  pnh_.param("stop_box_y_max", stop_box_y_max_, double(1.2));

  //  Add subscribers here
  laser_scan_sub_ =  nh_.subscribe(laser_scan_topic_, 1, &OccupancyGrid::laserscanCallback, this);
  vector_map_sub_ =  nh_.subscribe(vector_map_topic_, 1, &OccupancyGrid::vectormapCallback,this);
  fused_lidar_sub_ =  nh_.subscribe(fused_lidar_topic_, 1, &OccupancyGrid::fusedLidarCallback,this);
  detection_frt_rt_sub_ = nh_.subscribe("/darknet_ros/image_detector/objects", 1, &OccupancyGrid::detectionFrtRtCallback,this);
  cloud_cluster_sub_ = nh_.subscribe("/detection/lidar_detector/cloud_clusters", 1, &OccupancyGrid::cloudClusterCallback,this);
//  detection_frt_lt_=  new message_filters::Subscriber<autoware_msgs::DetectedObjectArray>(nh_, "/detection_fused_front_left", 1);
//  detection_frt_rt_=  new message_filters::Subscriber<autoware_msgs::DetectedObjectArray>(nh_, "/darknet_ros/image_detector/objects", 1);
//  detection_lt_=  new message_filters::Subscriber<autoware_msgs::DetectedObjectArray>(nh_, "/detection_fused_left", 1);
//  detection_rt_=  new message_filters::Subscriber<autoware_msgs::DetectedObjectArray>(nh_, "/detection_fused_right", 1);
//  detection_rr_=  new message_filters::Subscriber<autoware_msgs::DetectedObjectArray>(nh_, "/detection_fused_rear", 1);
//  cloud_cluster_=  new message_filters::Subscriber<autoware_msgs::CloudClusterArray>(nh_, "/detection/lidar_detector/cloud_clusters", 1);

  //  Add publishers here
  occupancy_map_pub_ = nh_.advertise<nav_msgs::OccupancyGrid>("/occupancy_map", 10);

  //Add subscriber synchronizer
//  detection_synchronizer_ = new message_filters::Synchronizer<SyncPolicyT>(SyncPolicyT(100),
//                                                                          *detection_frt_lt_,
//                                                                          *detection_frt_rt_,
//                                                                          *detection_lt_,
//                                                                          *detection_rt_,
//                                                                          *detection_rr_,
//                                                                          *cloud_cluster_);

//  detection_synchronizer_ = new message_filters::Synchronizer<SyncPolicyT>(SyncPolicyT(100),
//                                                                          
//                                                                          *detection_frt_rt_,
//                                                                          *cloud_cluster_);

////  detection_synchronizer_->registerCallback(boost::bind(&OccupancyGrid::CarStopCallback, this, _1,_2,_3,_4,_5,_6));

//  detection_synchronizer_->registerCallback(boost::bind(&OccupancyGrid::CarStopCallback, this, _1,_2));

  //  Add TimerEvent
  timer = nh_.createTimer(ros::Duration(0.1), &OccupancyGrid::createOccupancyGrid, this);

  ROS_INFO("Creating Occupancy Grid");
};

OccupancyGrid::~OccupancyGrid(){};

void OccupancyGrid::createOccupancyGrid(const ros::TimerEvent&)
{
  //if(current_laserscan_ && current_vector_map_) //simulation
  if(current_fused_lidar_ && current_detection_frtrt_ && current_cloud_cluster_) //with person detection
//  if(current_fused_lidar_) //regular car
  {
    nav_msgs::OccupancyGrid Occupancy;
    Occupancy.header.frame_id = occupancy_frame_;
    Occupancy.info.resolution = resolution_;
    Occupancy.info.width = int(map_width_/resolution_);
    Occupancy.info.height = int(map_height_/resolution_);
    Occupancy.info.origin.position.x = -map_width_/2;
    Occupancy.info.origin.position.y = -map_height_/2;
    Occupancy.info.origin.position.z = 0;
    Occupancy.info.origin.orientation.x = 0;
    Occupancy.info.origin.orientation.y = 0;
    Occupancy.info.origin.orientation.z = 0;
    Occupancy.info.origin.orientation.w = 1;

    int n = Occupancy.info.width *Occupancy.info.height;
    array = new int[n];
    for(int i = 0; i<n; i++)
    {
      array[i] = 0;
    }
    //laserscanToGrid();
    curbsToGrid();
    pointcloudToScan();
    carStop();

    for(int x = 0; x< n; x++)
    {
      occupancy_array.data.push_back(array[x]);
    }

    Occupancy.data = occupancy_array.data;
    occupancy_map_pub_.publish(Occupancy);
    ROS_INFO("Published OccupancyGrid");
    occupancy_array.data.clear();
    delete [] array;
    array = NULL;
  }
  else
  {
    ROS_INFO("Waiting for fused lidar/current detection frtrt/current cloud cluster");
    //ROS_INFO("Waiting for current_laserscan and current vector_map");
  }

      

}

void OccupancyGrid::pointcloudToScan()
{
  pcl::PointCloud<PointT>::Ptr out_cloud(new pcl::PointCloud<PointT>);
  pcl::fromROSMsg(*current_fused_lidar_, *out_cloud);
  for(size_t i = 0 ; i < out_cloud->points.size(); ++i)
  {
    double x = out_cloud->points[i].x;
    double y = out_cloud->points[i].y;
    double z = out_cloud->points[i].z;
    double distance = sqrt(pow((0.0-x),2)+pow((0.0-y),2));
    if (distance >= 4)
    {
      int x1 = (int)(x/resolution_)+map_width_;
      int y1 = (int)(y/resolution_)+map_height_;
      int position = x1+map_width_/resolution_*y1;
      array[position] = 50;
    }
  }
  //std::cout<<"in func"<<std::endl;
}

void OccupancyGrid::laserscanToGrid()
{
  float polar_angle, x, y;
  int count{0};
  double inf = std::numeric_limits<double>::infinity();
  for(auto range: current_laserscan_->ranges)
  {
    polar_angle = current_laserscan_->angle_min + (count*current_laserscan_->angle_increment);
    x = range*cos(polar_angle);
    y = range*sin(polar_angle);
    if(insideBoundaries(x,y))
    {
      ROS_INFO("Inside Boundaries");
      int x1 = (int)(x/resolution_)+map_width_;
      int y1 = (int)(y/resolution_)+map_height_;
      int position = x1+map_width_/resolution_*y1;
      array[position] = 50;
    }
    count++;
  }
}

void OccupancyGrid::curbsToGrid()
{
  if(current_vector_map_ != nullptr)
  {
    for(auto curb: current_vector_map_->markers)
    {
      for(auto point: curb.points)
      {
        auto current_x = point.x;
        auto current_y = point.y;
        geometry_msgs::PoseStamped waypoint = createNewWaypoint(current_x, current_y, vector_map_frame_);
        geometry_msgs::Pose transformed_pose;
        //we now have transformed pose in velodyne frames for the curb point
        transformed_pose = lookupTF(waypoint, occupancy_frame_, vector_map_frame_);
        auto x = transformed_pose.position.x;
        auto y = transformed_pose.position.y;
        if(insideBoundaries(x,y))
        {
          int x1 = int(x/resolution_)+map_width_;
          int y1 = int(y/resolution_)+map_height_;
          int position = x1+map_width_/resolution_*y1;
          array[position] = 50;
        }
      }
    }
  }

}

double OccupancyGrid::fitToResolution(float float_val)
{
  //snap the floats to nearest res value
  double snapped_val;
  snapped_val = double(int(float_val /resolution_ + 0.5)) *resolution_;
  return snapped_val;
}

bool OccupancyGrid::insideBoundaries(float x, float y)
{
  double inf = std::numeric_limits<double>::infinity();
  if(x >= -map_width_/2 && x <= map_width_/2 && x != inf && x!= -inf)
  {
    if(y >= -map_height_/2 && y <= map_height_/2 && y != inf && y != -inf)
      return true;
    else
      return false;
  }
  else
    return false;
}

geometry_msgs::PoseStamped OccupancyGrid::createNewWaypoint(double current_x, double current_y, std::string frame)
{
    geometry_msgs::PoseStamped waypoint;
    waypoint.header.frame_id = frame; //"map"
    waypoint.pose.position.x = current_x;
    waypoint.pose.position.y = current_y;
    waypoint.pose.position.z = 0;
    waypoint.pose.orientation.x = 0.0;
    waypoint.pose.orientation.y = 0.0;
    waypoint.pose.orientation.z = 0.0;
    waypoint.pose.orientation.w = 1.0;
    return waypoint;
}

//Generic lookup transform function
geometry_msgs::Pose OccupancyGrid::lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original)
{
    geometry_msgs::Pose transformed_pose;
    tf::Transform dest_to_orig;
    tf::poseMsgToTF(initial_pt.pose, dest_to_orig);
    tf::StampedTransform req_to_dest;
    try
    {
      listener_.lookupTransform(destination, original, ros::Time(0), req_to_dest);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }
    tf::Transform req_to_orig;
    req_to_orig = req_to_dest * dest_to_orig;
    tf::poseTFToMsg(req_to_orig, transformed_pose);

    return transformed_pose;
}

//void OccupancyGrid::CarStopCallback( const autoware_msgs::DetectedObjectArray::ConstPtr msg_frt_lt,
//                         const autoware_msgs::DetectedObjectArray::ConstPtr msg_frt_rt,
//                         const autoware_msgs::DetectedObjectArray::ConstPtr msg_lt,
//                         const autoware_msgs::DetectedObjectArray::ConstPtr msg_rt,
//                         const autoware_msgs::DetectedObjectArray::ConstPtr msg_rr,
//                         const autoware_msgs::CloudClusterArray::ConstPtr euc_clusters)

void OccupancyGrid::carStop()
{
//ROS_INFO("Entered CarStopCallback");
  autoware_msgs::DetectedObjectArray person_msg;
//  autoware_msgs::DetectedObject stop_msg;

//  for(auto frt_lt:msg_frt_lt->objects)
//  {
//    if(frt_lt.label == "person")
//    {
//      ROS_INFO("detected person1");
//      person_msg.objects.push_back(frt_lt);
//    }
//  }

  for(auto frt_rt:current_detection_frtrt_->objects)
  {
    if(frt_rt.label == "person" && frt_rt.x>1450 && frt_rt.x<1780)
    {
//      ROS_INFO("detected person2");
      person_msg.objects.push_back(frt_rt);
    }
//    else if(frt_rt.label == "stop sign" && 2.0<frt_rt.pose.position.x < 5.0 && -4.0 < frt_rt.pose.position.y < -1.5)
//         {
//           stop_time_ = ros::Time::now();
//           stop_counter_ =stop_counter_ + 1; 
//           stop_msg.objects.label = frt_rt.label;
//           stop_msg.objects.header.stamp = ros::Time::now();
//           stop_msg.objects.pose.position.x = frt_rt.pose.position.x;
//           stop_msg.objects.pose.position.y = frt_rt.pose.position.y;
//           stop_msg.objects.pose.position.z = frt_rt.pose.position.z;
//         }

  }

//  for(auto rt:msg_rt->objects)
//  {
//    if(rt.label == "person")
//    {
//      person_msg.objects.push_back(rt);
//    }
//  }

//  for(auto lt:msg_lt->objects)
//  {
//    if(lt.label == "person")
//    {
//      person_msg.objects.push_back(lt);
//    }
//  }

//  for(auto rr:msg_rr->objects)
//  {
//    if(rr.label == "person")
//    {
//      person_msg.objects.push_back(rr);
//    }
//  }
  double cluster_x_min, cluster_x_max, cluster_y_min, cluster_y_max;
  int count = 0;
  for(auto i:person_msg.objects)
  {
    for(auto j:current_cloud_cluster_->clusters)
    {
      cluster_x_min = j.centroid_point.point.x - (j.dimensions.x)/2;
      cluster_x_max = j.centroid_point.point.x + (j.dimensions.x)/2;
      cluster_y_min = j.centroid_point.point.y - (j.dimensions.y)/2;
      cluster_y_max = j.centroid_point.point.y + (j.dimensions.y)/2;
//      if(j.centroid_point.point.y<2.0 && j.centroid_point.point.y>-2.0){
//      ROS_INFO("centroid_x = %f",j.centroid_point.point.x);
//      ROS_INFO("centroid_y = %f",j.centroid_point.point.y);
//      ROS_INFO("dimensions_x = %f",j.dimensions.x/2);
//      ROS_INFO("cluster_x_min = %f",cluster_x_min);}
//      ROS_INFO("width = %f",map_width_);}
//      if(j.centroid_point.point.x > 0.0 && i.label == "person" && j.centroid_point.point.x < stop_box_x_max_ && j.centroid_point.point.y > stop_box_y_min_ && j.centroid_point.point.y < stop_box_y_max_ )
        if(j.centroid_point.point.y<2.0 && i.label == "person" && j.centroid_point.point.y>-3.0 && j.centroid_point.point.x>3.0 && j.centroid_point.point.x<12.0)
      {
//        if(0.0 < i.pose.position.x && i.pose.position.x < stop_box_x_max_ && i.pose.position.y > stop_box_y_min_ && i.pose.position.y < stop_box_y_max_)
//        {
          int x1 = (int)(j.centroid_point.point.x/(resolution_))+map_height_;
          int y1 = (int)(j.centroid_point.point.y/resolution_)+map_width_;
          int c = (int)(j.centroid_point.point.x/resolution_)+(map_height_);
          for(int r = 0; r<map_width_/resolution_; r++)
          {
//            x1=0;
//            y1=0;
//            int position = map_height_;
            int position = (r*(map_height_/resolution_))+c;
            array[position] = 50;

            ROS_INFO("STOP: PEDESTRIAN AHEAD");
//            ROS_INFO("width = %d",map_width_);
//            ROS_INFO("height = %d",map_height_);
//            ROS_INFO("centroid_y = %f",j.centroid_point.point.y);
//            ROS_INFO("dimensions_x = %f",j.dimensions.x/2);
//            ROS_INFO("cluster_x_min = %f",cluster_x_min);
              count = 0;
          }
        }
        else count = 1;
//      }
    }
  }

//  if(stop_counter_ == 3 && ros::Time::now() - stop_time_ < 1)
//  {
//    for(int k=0; k<=map_height_; k++)
//          {
//            int x1 = (int)(stop_msg.objects.pose.position.x/resolution_)+map_width_;
//            int y1 = (int)(k/resolution_)+map_height_;
//            int position = x1+map_width_/resolution_*y1;
//            array[position] = 50;
//          }
//  }
}

//Add Callbacks here
void OccupancyGrid::laserscanCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  current_laserscan_ = std::make_shared<sensor_msgs::LaserScan>(*msg);
}
void OccupancyGrid::vectormapCallback( const visualization_msgs::MarkerArray::ConstPtr& msg)
{
  current_vector_map_ = std::make_shared<visualization_msgs::MarkerArray>(*msg);
}
void OccupancyGrid::fusedLidarCallback( const sensor_msgs::PointCloud2::ConstPtr& msg)
{
  current_fused_lidar_ = std::make_shared<sensor_msgs::PointCloud2>(*msg);
}
void OccupancyGrid::detectionFrtRtCallback(const autoware_msgs::DetectedObjectArray::ConstPtr& msg)
{
  current_detection_frtrt_ = std::make_shared<autoware_msgs::DetectedObjectArray>(*msg);
}
void OccupancyGrid::cloudClusterCallback(const autoware_msgs::CloudClusterArray::ConstPtr& msg)
{
  current_cloud_cluster_ = std::make_shared<autoware_msgs::CloudClusterArray>(*msg);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "occupancy_grid");
  OccupancyGrid obj;
  ros::spin();
  return 0;
};
