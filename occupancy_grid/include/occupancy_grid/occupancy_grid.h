// #ifndef PROJECT_OCCUPANCY_GRID_H
// #define PROJECT_OCCUPANCY_GRID_H
//
// #define __APP_NAME__ "occupancy_grid"

#include <iostream>
#include <vector>
#include <cmath>
#include <ros/ros.h>
#include <string>
#include <tuple>
#include <math.h>
#include <array>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GridCells.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Bool.h>
#include <autoware_msgs/LaneArray.h>
#include <autoware_msgs/Waypoint.h>
#include <autoware_msgs/DetectedObjectArray.h>
#include <autoware_msgs/CloudClusterArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Point.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <sensor_msgs/PointCloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>



class OccupancyGrid{
  public:
    OccupancyGrid();
    ~OccupancyGrid();

  private:
    //  Add node handle here
    ros::NodeHandle nh_, pnh_;;

    //  Add ROS Parameters here
    std::string laser_scan_topic_;
    std::string vector_map_topic_;
    std::string fused_lidar_topic_;
    std::string occupancy_frame_;
    std::string vector_map_frame_;
    int map_width_;
    int map_height_;
    int stop_counter_ = 0;
    double resolution_;
    double stop_box_x_max_;
    double stop_box_y_max_;
    double stop_box_y_min_;
//    ros::Timer stop_time_;

    typedef pcl::PointXYZ              PointT;

    //  Add subscribers here
    ros::Subscriber laser_scan_sub_;
    ros::Subscriber vector_map_sub_;
    ros::Subscriber fused_lidar_sub_;
    ros::Subscriber detection_frt_rt_sub_;
    ros::Subscriber cloud_cluster_sub_;

    //  Add Publishers here
    ros::Publisher occupancy_map_pub_;

    //  Add Timer Events here
    ros::Timer timer;

    //  Add private member variables here
    std_msgs::Int8MultiArray occupancy_array;
    int* array = NULL;

    //Add subscriber synchronizer
    typedef
//  	message_filters::sync_policies::ApproximateTime<autoware_msgs::DetectedObjectArray, autoware_msgs::DetectedObjectArray,
//          autoware_msgs::DetectedObjectArray, autoware_msgs::DetectedObjectArray, autoware_msgs::DetectedObjectArray, autoware_msgs::CloudClusterArray> SyncPolicyT;

message_filters::sync_policies::ApproximateTime<autoware_msgs::DetectedObjectArray,
           autoware_msgs::CloudClusterArray> SyncPolicyT;

//    message_filters::Subscriber<autoware_msgs::DetectedObjectArray> *detection_frt_lt_, *detection_frt_rt_, *detection_lt_, *detection_rt_, *detection_rr_;
//    message_filters::Subscriber<autoware_msgs::CloudClusterArray> *cloud_cluster_;
//    message_filters::Synchronizer<SyncPolicyT>    *detection_synchronizer_;

    message_filters::Subscriber<autoware_msgs::DetectedObjectArray> *detection_frt_lt_, *detection_frt_rt_;
    message_filters::Subscriber<autoware_msgs::CloudClusterArray> *cloud_cluster_;
    message_filters::Synchronizer<SyncPolicyT>    *detection_synchronizer_;


    // Add private member function declarations here.
    void createOccupancyGrid(const ros::TimerEvent&);
    void laserscanToGrid();
    void curbsToGrid();
    void pointcloudToScan();
    void carStop();

    //  Add Helper functions here
    double fitToResolution(float float_val);
    bool insideBoundaries(float x, float y);
    geometry_msgs::PoseStamped createNewWaypoint(double current_x, double current_y, std::string frame);


    /* Add shared pointers here*/
    std::shared_ptr <sensor_msgs::LaserScan> current_laserscan_;
    std::shared_ptr <visualization_msgs::MarkerArray> current_vector_map_;
    std::shared_ptr <sensor_msgs::PointCloud2> current_fused_lidar_;
    std::shared_ptr <autoware_msgs::DetectedObjectArray> current_detection_frtrt_;
    std::shared_ptr <autoware_msgs::CloudClusterArray> current_cloud_cluster_;


    /*Add ROS callbacks here */
    void laserscanCallback(const sensor_msgs::LaserScan::ConstPtr& msg);
    void vectormapCallback( const visualization_msgs::MarkerArray::ConstPtr& msg);
    void fusedLidarCallback( const sensor_msgs::PointCloud2::ConstPtr& msg);
    void detectionFrtRtCallback(const autoware_msgs::DetectedObjectArray::ConstPtr& msg);
    void cloudClusterCallback(const autoware_msgs::CloudClusterArray::ConstPtr& msg);


//    void CarStopCallback( const autoware_msgs::DetectedObjectArray::ConstPtr msg_frt_lt,
//                             const autoware_msgs::DetectedObjectArray::ConstPtr msg_frt_rt,
//                             const autoware_msgs::DetectedObjectArray::ConstPtr msg_lt,
//                             const autoware_msgs::DetectedObjectArray::ConstPtr msg_rt,
//                             const autoware_msgs::DetectedObjectArray::ConstPtr msg_rr,
//                             const autoware_msgs::CloudClusterArray::ConstPtr euc_clusters);

//    void CarStopCallback(const autoware_msgs::DetectedObjectArray::ConstPtr msg_frt_rt,

//                             const autoware_msgs::CloudClusterArray::ConstPtr euc_clusters);

    //  Add TransformListener and transform functions here
    tf::TransformListener listener_;
    geometry_msgs::Pose lookupTF(geometry_msgs::PoseStamped initial_pt, std::string destination, std::string original);
};
