#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <ros/ros.h>
#include <string>
#include <fstream>
#include <tuple>
#include <math.h>
#include <iomanip>
#include <stdio.h>
#include <nav_msgs/OccupancyGrid.h>
#include <std_msgs/Int8.h> 
#include <std_msgs/Int32.h> 
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Bool.h>
#include <grid_map_msgs/GridMap.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <limits>
#include "Eigen/Dense"
#include <autoware_msgs/LaneArray.h>
#include <autoware_msgs/Waypoint.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <tf/transform_listener.h>
#include "tf2_msgs/TFMessage.h"

#include "tf2_ros/transform_listener.h"
#include "tf2_ros/message_filter.h"
#include "message_filters/subscriber.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include <visualization_msgs/MarkerArray.h>

using namespace std::chrono;
using namespace grid_map;


/*
*Instructions to plot the potential in gnuplot
* open a new terminal
* $ gnuplot
* $ set pm3d //enable color fill with legend
* $ set hidden3d //show 3rd axis
* $ set dgrid3d //see how to give row, column number inputs
* $ set dgrid3d 50,50 qnorm 2
* $ splot 'data_file' with lines //create a mesh, not just points

*/

class PotentialField{
  public:
    PotentialField();
    ~PotentialField();

  private:
    ros::NodeHandle nh_;
  
    // Add subscribers here
    ros::Subscriber cluster_map_sub_;
    ros::Subscriber occupancy_grid_sub_;
    ros::Subscriber waypoint_sub_;  
    ros::Subscriber index_sub_;
    ros::Subscriber curb_sub_;
    
    // Add publishers here
    ros::Publisher publisher;
    ros::Publisher marker_pub_;
    ros::Publisher obstacle_pub_;
    ros::Publisher vis_pub_start_;
    ros::Publisher vis_pub_goal_;
    ros::Publisher inflation_pub_;
    ros::Publisher vis_pub_flowfield_;
    ros::Publisher pot_field_replan_pub_;
    ros::Publisher pot_field_path_pub_;
    ros::Publisher feasible_path_pub_;
    
    

    // Add private member variables here


    int nMapWidth{};
    int nMapHeight{};
    float res{}; //resolution of occupancy grid and related to your motion model and flow
    double k_att{2};
    double k_rep{1500};
    double robot_radius {3};

    std::tuple<int, int, double> start_node{0, 0, 0};
    std::tuple<int, int, double, double, double> goal_node{0, 0, 1, 0, 0};
    std::vector<std::tuple<int,int,double>> flowfield {};
    std::vector<std::tuple<int,int>> obstacles {};
    std::vector<std::tuple<int,int>> inflations {};
    std::vector<std::vector<float>> motion {};
    std::vector<std::vector<float>> motion1 {};
    std::vector<std::vector<float>> motion2 {};
    std::vector<std::tuple<int,int,double>> path_;

    // Add private member function declarations here
    void createWaveFront();
    void createFlowField();
    void getMotionModel();
    void planner();

    void repulsivePotential(std::tuple<int, int, double> &current_node);
    double getDistance(const std::tuple<int, int, double> &node1, 
                       const std::tuple<int, int, double> &node2);
    double getDistance(const std::tuple<int, int, double> &node1, 
                       const std::tuple<int, int> &node2);
                       
    void attractivePotential(std::tuple<int, int, double> &current_node);



    void printFlowField();
    void vizPath();
    void vizObstacles();
    void vizStartAndGoal();
    void viz();
    void vizInflation();
    void vizFlowField();
    
    /*shared pointers*/
    std::shared_ptr <nav_msgs::OccupancyGrid> current_cluster_;
    std::shared_ptr <nav_msgs::OccupancyGrid> current_occupancy_;
    std::shared_ptr <autoware_msgs::LaneArray> current_lanes_;
    std::shared_ptr <std_msgs::Int32> current_closest_index_;
    std::shared_ptr <visualization_msgs::MarkerArray> current_curbs_;
    
    /*functions relating to ROS*/
    void createFlowFieldROS();
    void createInflationLayer();
    void findGoalWaypointIndex();
    void createWaypoints();
    void addCurbsAsObstacles();
    
    geometry_msgs::Pose lookupTF(geometry_msgs::PoseStamped initial_pt);
    geometry_msgs::Pose lookupTFReverse(geometry_msgs::PoseStamped initial_pt);
//    void visualization();
    
    std::tuple<int, int> fitToResolution(std::tuple<double,double>);
    
    
    /*ROS callbacks*/
    void clusterCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
    void occupanyCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
    void waypointCallback(const autoware_msgs::LaneArray::ConstPtr& msg);
    void closestWaypointIndexCallback(const std_msgs::Int32::ConstPtr& msg);
    void curbCallback(const visualization_msgs::MarkerArray::ConstPtr& msg);
    
    //  TransformListener for transforms of waypoints and goal position
    tf::TransformListener listener_;

};
