#!/usr/bin/env python
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import cv2
import rospy 
from sensor_msgs.msg import Image,LaserScan,Joy
from geometry_msgs.msg import Pose,Twist
import sys
from autoware_msgs.msg import VehicleCmd,DetectedObjectArray

 
def Subscriber1():
     rospy.Subscriber('/vehicle_cmd',VehicleCmd,vehicle_callback)

def Subscriber2():
     rospy.Subscriber('/detection/image_detector/objects',DetectedObjectArray,detection_callback)

def detection_callback(data):
     rospy.loginfo('Receiving Detection data')
     if data.objects[0].label == 'person':
         breadth = data.objects[0].width
         length = data.objects[0].height
         ROI = length*breadth
         global flag
         rospy.loginfo(ROI)
         if ROI > 10:
            flag = 1
         else:
            flag = 0
        
def vehicle_callback(data):

    rospy.loginfo("Receiving VehicleCmd Information")
    # populate all fields
    msg =Joy()
    if flag == 1:
       msg.axes = [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    else:
       msg.axes = [0.0,data.ctrl_cmd.linear_velocity,0.0,data.ctrl_cmd.steering_angle,0.0,0.0,0.0]
    # put map conditions here
    vehicle_pub = rospy.Publisher('/joy',Joy,queue_size=10)
    vehicle_pub.publish(msg)
    

if __name__ == '__main__':
    rospy.init_node('converted')
    Subscriber1()
    Subscriber2() 
    r = rospy.Rate(40)
    while not rospy.is_shutdown():
        # r.sleep()
        rospy.spin()
